/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyController
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import io.yoosh.app.R
import io.yoosh.app.core.extensions.cleanup
import io.yoosh.app.core.extensions.exhaustive
import io.yoosh.app.core.platform.AppBaseFragment
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.databinding.FragmentSpaceHomeListBinding
import io.yoosh.app.features.home.HomeActivitySharedAction
import io.yoosh.app.features.home.HomeSharedActionViewModel
import io.yoosh.app.space
import org.matrix.android.sdk.api.session.group.model.GroupSummary
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import javax.inject.Inject

class SpaceHomeListFragment @Inject constructor(
        val spaceListViewModelFactory: SpaceHomeListViewModel.Factory,
        private val spaceController: SpaceHomeSummaryController
) : AppBaseFragment<FragmentSpaceHomeListBinding>(), SpaceHomeSummaryController.Callback {

    private lateinit var sharedActionViewModel: HomeSharedActionViewModel
    private val viewModel: SpaceHomeListViewModel by fragmentViewModel()

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentSpaceHomeListBinding {
        return FragmentSpaceHomeListBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedActionViewModel = activityViewModelProvider.get(HomeSharedActionViewModel::class.java)
        spaceController.callback = this
        views.groupListView.configureHorizontalWith(spaceController, showDivider = true,
                space = 0,
                skipFirst = true
        )
        viewModel.observeViewEvents {
            when (it) {
                is SpaceListViewEvents.OpenSpaceSummary -> sharedActionViewModel.post(HomeActivitySharedAction.OpenSpacePreview(it.id))
                is SpaceListViewEvents.OpenSpace        -> sharedActionViewModel.post(HomeActivitySharedAction.OpenGroup)
                is SpaceListViewEvents.AddSpace         -> sharedActionViewModel.post(HomeActivitySharedAction.AddSpace(null))
                is SpaceListViewEvents.OpenSpaceInvite  -> sharedActionViewModel.post(HomeActivitySharedAction.OpenSpaceInvite(it.id))
            }.exhaustive
        }

        sharedActionViewModel
                .observe()
                .subscribe { sharedAction ->
                    when (sharedAction) {
                        HomeActivitySharedAction.OpenRootSpaces -> handleShowRootSpaces()
                        else                                    -> Unit
                    }
                }.exhaustive
    }

    private fun handleShowRootSpaces() {
        viewModel.handle(SpaceListAction.SelectSpace(null))
    }

    override fun onDestroyView() {
        spaceController.callback = null
        views.groupListView.cleanup()
        super.onDestroyView()
    }

    override fun invalidate() = withState(viewModel) { state ->
        spaceController.update(state)
    }

    override fun onSpaceSelected(spaceSummary: RoomSummary?) {
        sharedActionViewModel.post(HomeActivitySharedAction.SelectSpace(spaceSummary))
        viewModel.handle(SpaceListAction.SelectSpace(spaceSummary))
    }

    override fun onSpaceInviteSelected(spaceSummary: RoomSummary) {
        viewModel.handle(SpaceListAction.OpenSpaceInvite(spaceSummary))
    }

    override fun onSpaceSettings(spaceSummary: RoomSummary) {
        sharedActionViewModel.post(HomeActivitySharedAction.ShowSpaceSettings(spaceSummary.roomId))
    }

    override fun onToggleExpand(spaceSummary: RoomSummary) {
        viewModel.handle(SpaceListAction.ToggleExpand(spaceSummary))
    }

    override fun onGroupSelected(groupSummary: GroupSummary?) {
        viewModel.handle(SpaceListAction.SelectLegacyGroup(groupSummary))
    }
}

/**
 * Apply a Vertical LinearLayout Manager to the recyclerView and set the adapter from the epoxy controller
 */
fun RecyclerView.configureHorizontalWith(epoxyController: EpoxyController,
                                         itemAnimator: RecyclerView.ItemAnimator? = null,
                                         viewPool: RecyclerView.RecycledViewPool? = null,
                                         showDivider: Boolean = true,
                                         disableItemAnimation: Boolean = false,
                                         space: Int = 50,
                                         skipFirst: Boolean = false) {
    setRecycledViewPool(viewPool)
    if (disableItemAnimation) {
        this.itemAnimator = null
    } else {
        itemAnimator?.let { this.itemAnimator = it }
    }
    if (showDivider) {
        addItemDecoration(HorizontalSpaceItemDecoration(space, skipFirst = skipFirst))
    }
    adapter = epoxyController.adapter
}

class HorizontalSpaceItemDecoration(private val space: Int, private val skipFirst: Boolean = false) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.right = space
        // quick fix
        outRect.bottom = space

        // Add top margin only for the first item to avoid double space between items
        if (parent.getChildAdapterPosition(view) == 0 && !skipFirst) {
            outRect.left = space
        }
    }
}
