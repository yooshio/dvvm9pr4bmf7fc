/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.list

import androidx.recyclerview.widget.DiffUtil
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagedListEpoxyController
import io.yoosh.app.AppStateHandler
import io.yoosh.app.core.epoxy.TimelineEmptyItem_
import io.yoosh.app.core.utils.createUIHandler
import io.yoosh.app.features.home.RoomListDisplayMode
import org.matrix.android.sdk.api.session.room.members.ChangeMembershipState
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import timber.log.Timber

class RoomSummaryPagedController(
        private val roomSummaryItemFactory: RoomSummaryItemFactory,
        private val displayMode: RoomListDisplayMode
) : PagedListEpoxyController<RoomSummary>(
        // Important it must match the PageList builder notify Looper
        modelBuildingHandler = createUIHandler(),
        itemDiffCallback = itemDiff
), CollapsableControllerExtension {

    companion object {
        val itemDiff = object : DiffUtil.ItemCallback<RoomSummary>() {
            override fun areItemsTheSame(oldItem: RoomSummary, newItem: RoomSummary): Boolean {
                return false
            }

            override fun areContentsTheSame(oldItem: RoomSummary, newItem: RoomSummary): Boolean {
                return false
            }
        }
    }

    var listener: RoomListListener? = null

    var appStateHandler: AppStateHandler? = null

    var roomChangeMembershipStates: Map<String, ChangeMembershipState>? = null
        set(value) {
            field = value
            // ideally we could search for visible models and update only those
            requestForcedModelBuild()
        }

    override var collapsed = false
        set(value) {
            if (field != value) {
                field = value
                requestForcedModelBuild()
            }
        }

    override fun addModels(models: List<EpoxyModel<*>>) {
        if (collapsed) {
            super.addModels(emptyList())
        } else {
            listener?.onAddModels()
            super.addModels(models)
        }
    }

    override fun buildItemModel(currentPosition: Int, item: RoomSummary?): EpoxyModel<*> {
        item ?: return RoomSummaryItemPlaceHolder_().apply { id(currentPosition) }
//        if (displayMode == RoomListDisplayMode.ROOMS) {
//            val spaceId = appStateHandler?.safeActiveSpaceId()
//
//            if (item.flattenParentIds.lastOrNull() != spaceId) return TimelineEmptyItem_().apply { id("timeline-$currentPosition") }
//        }

        return roomSummaryItemFactory.create(item, roomChangeMembershipStates.orEmpty(), emptySet(), listener)
                ?: RoomSummaryItemPlaceHolder_().apply { id(currentPosition) }
    }

    fun spaceIdUpdated() {
        if (displayMode == RoomListDisplayMode.ROOMS) requestForcedModelBuild()
    }
}
