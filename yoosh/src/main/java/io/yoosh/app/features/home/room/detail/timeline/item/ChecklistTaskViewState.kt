/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.item

sealed class ChecklistTaskViewState(open val taskId: String,
                                    open val taskAnswer: String) {
    /**
     * Represents a checklist that is not sent to the server yet.
     */
    data class ChecklistSending(override val taskId: String,
                                override val taskAnswer: String
    ) : ChecklistTaskViewState(taskId, taskAnswer)

    /**
     * Represents a checklist that user already voted.
     */
    data class ChecklistReady(override val taskId: String,
                              override val taskAnswer: String,
                              val isSelected: Boolean
    ) : ChecklistTaskViewState(taskId, taskAnswer)
}
