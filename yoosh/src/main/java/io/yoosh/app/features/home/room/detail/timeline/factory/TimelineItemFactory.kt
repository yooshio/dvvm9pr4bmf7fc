/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.factory

import io.yoosh.app.core.epoxy.AppEpoxyModel
import org.matrix.android.sdk.api.session.events.model.EventType
import timber.log.Timber
import javax.inject.Inject

class TimelineItemFactory @Inject constructor(private val messageItemFactory: MessageItemFactory,
                                              private val noticeItemFactory: NoticeItemFactory) {

    /**
     * Reminder: nextEvent is older and prevEvent is newer.
     */
    fun create(params: TimelineItemFactoryParams): AppEpoxyModel<*>? {
        val event = params.event
        val computedModel = try {
            when (event.root.getClearType()) {
                // Message itemsX
                EventType.MESSAGE          -> messageItemFactory.create(params)
                EventType.CHECKLIST_CREATE -> messageItemFactory.create(params)
//                EventType.CHECKLIST_UPDATE -> noticeItemFactory.create(params)
//                EventType.STATE_ROOM_TOMBSTONE,
//                EventType.STATE_ROOM_NAME,
//                EventType.STATE_ROOM_TOPIC,
//                EventType.STATE_ROOM_AVATAR,
                EventType.STATE_ROOM_MEMBER,
//                EventType.STATE_ROOM_CANONICAL_ALIAS,
//                EventType.STATE_ROOM_JOIN_RULES,
//                EventType.STATE_ROOM_HISTORY_VISIBILITY,
//                EventType.STATE_ROOM_SERVER_ACL,
//                EventType.STATE_ROOM_GUEST_ACCESS,
//                EventType.STATE_ROOM_ALIASES,
//                EventType.KEY_VERIFICATION_ACCEPT,
//                EventType.KEY_VERIFICATION_START,
//                EventType.KEY_VERIFICATION_KEY,
//                EventType.KEY_VERIFICATION_READY,
//                EventType.KEY_VERIFICATION_MAC,
//                EventType.STATE_SPACE_CHILD,
//                EventType.STATE_SPACE_PARENT,
//                EventType.STATE_ROOM_POWER_LEVELS
                EventType.STATE_ROOM_THIRD_PARTY_INVITE -> noticeItemFactory.create(params)
//                EventType.STATE_ROOM_ENCRYPTION -> encryptionItemFactory.create(params)
                // State room create
//                EventType.STATE_ROOM_CREATE     -> roomCreateItemFactory.create(params)
                // Crypto
//                EventType.ENCRYPTED             -> {
//                    if (event.root.isRedacted()) {
//                        // Redacted event, let the MessageItemFactory handle it
//                        messageItemFactory.create(params)
//                    } else {
//                        encryptedItemFactory.create(params)
//                    }
//                }
//                EventType.KEY_VERIFICATION_CANCEL,
//                EventType.KEY_VERIFICATION_DONE -> {
//                    verificationConclusionItemFactory.create(params)
//                }
                // Unhandled event types
                else                                    -> {
                    null
                }
            }
        } catch (throwable: Throwable) {
            Timber.e(throwable, "failed to create message item")
            null
        }
        return computedModel
    }
}
