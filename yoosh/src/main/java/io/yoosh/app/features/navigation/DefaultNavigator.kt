/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import android.view.Window
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.util.Pair
import androidx.core.view.ViewCompat
import io.yoosh.app.AppStateHandler
import io.yoosh.app.R
import io.yoosh.app.core.di.ActiveSessionHolder
import io.yoosh.app.core.error.fatalError
import io.yoosh.app.core.platform.AppBaseActivity
import io.yoosh.app.core.utils.toast
import io.yoosh.app.features.crypto.keysbackup.settings.KeysBackupManageActivity
import io.yoosh.app.features.crypto.keysbackup.setup.KeysBackupSetupActivity
import io.yoosh.app.features.crypto.recover.BootstrapBottomSheet
import io.yoosh.app.features.crypto.recover.SetupMode
import io.yoosh.app.features.crypto.verification.SupportedVerificationMethodsProvider
import io.yoosh.app.features.crypto.verification.VerificationBottomSheet
import io.yoosh.app.features.home.room.detail.RoomDetailActivity
import io.yoosh.app.features.home.room.detail.RoomDetailArgs
import io.yoosh.app.features.home.room.detail.search.SearchActivity
import io.yoosh.app.features.home.room.detail.search.SearchArgs
import io.yoosh.app.features.home.room.filtered.FilteredRoomsActivity
import io.yoosh.app.features.invite.InviteUsersToRoomActivity
import io.yoosh.app.features.login.LoginActivity
import io.yoosh.app.features.login.LoginConfig
import io.yoosh.app.features.matrixto.MatrixToBottomSheet
import io.yoosh.app.features.media.AttachmentData
import io.yoosh.app.features.media.BigImageViewerActivity
import io.yoosh.app.features.media.AppAttachmentViewerActivity
import io.yoosh.app.features.checklist.create.CreateChecklistActivity
import io.yoosh.app.features.checklist.create.CreateChecklistArgs
import io.yoosh.app.features.roomdirectory.RoomDirectoryData
import io.yoosh.app.features.roomdirectory.createroom.CreateRoomActivity
import io.yoosh.app.features.roomdirectory.roompreview.RoomPreviewActivity
import io.yoosh.app.features.roomdirectory.roompreview.RoomPreviewData
import io.yoosh.app.features.roomprofile.RoomProfileActivity
import io.yoosh.app.features.settings.AppPreferences
import io.yoosh.app.features.settings.AppSettingsActivity
import io.yoosh.app.features.share.SharedData
import io.yoosh.app.features.spaces.SpacePreviewActivity
import io.yoosh.app.features.spaces.manage.ManageType
import io.yoosh.app.features.spaces.manage.SpaceManageActivity
import io.yoosh.app.features.terms.ReviewTermsActivity
import io.yoosh.app.features.widgets.WidgetActivity
import io.yoosh.app.features.widgets.WidgetArgsBuilder
import org.matrix.android.sdk.api.session.crypto.verification.IncomingSasVerificationTransaction
import org.matrix.android.sdk.api.session.room.model.roomdirectory.PublicRoom
import org.matrix.android.sdk.api.session.terms.TermsService
import org.matrix.android.sdk.api.session.widgets.model.Widget
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DefaultNavigator @Inject constructor(
        private val sessionHolder: ActiveSessionHolder,
        private val vectorPreferences: AppPreferences,
        private val widgetArgsBuilder: WidgetArgsBuilder,
        private val appStateHandler: AppStateHandler,
        private val supportedVerificationMethodsProvider: SupportedVerificationMethodsProvider
) : Navigator {

    override fun openLogin(context: Context, loginConfig: LoginConfig?, flags: Int) {
        val intent = LoginActivity.newIntent(context, loginConfig)
        intent.addFlags(flags)
        context.startActivity(intent)
    }

    override fun openRoom(context: Context, roomId: String, eventId: String?, buildTask: Boolean) {
        if (sessionHolder.getSafeActiveSession()?.getRoom(roomId) == null) {
            fatalError("Trying to open an unknown room $roomId", vectorPreferences.failFast())
            return
        }
        val args = RoomDetailArgs(roomId, eventId)
        val intent = RoomDetailActivity.newIntent(context, args)
        startActivity(context, intent, buildTask)
    }

    override fun switchToSpace(context: Context, spaceId: String, postSwitchSpaceAction: Navigator.PostSwitchSpaceAction) {
        if (sessionHolder.getSafeActiveSession()?.getRoomSummary(spaceId) == null) {
            fatalError("Trying to open an unknown space $spaceId", vectorPreferences.failFast())
            return
        }
        appStateHandler.setCurrentSpace(spaceId)
        when (postSwitchSpaceAction) {
            Navigator.PostSwitchSpaceAction.None                 -> {
                // go back to home if we are showing room details?
                // This is a bit ugly, but the navigator is supposed to know about the activity stack
                if (context is RoomDetailActivity) {
                    context.finish()
                }
            }
            Navigator.PostSwitchSpaceAction.OpenAddExistingRooms -> {
                startActivity(context, SpaceManageActivity.newIntent(context, spaceId, ManageType.AddRooms), false)
            }
            is Navigator.PostSwitchSpaceAction.OpenDefaultRoom   -> {
                val args = RoomDetailArgs(
                        postSwitchSpaceAction.roomId,
                        eventId = null,
                        openShareSpaceForId = spaceId.takeIf { postSwitchSpaceAction.showShareSheet }
                )
                val intent = RoomDetailActivity.newIntent(context, args)
                startActivity(context, intent, false)
            }
        }
    }

    override fun openSpacePreview(context: Context, spaceId: String) {
        startActivity(context, SpacePreviewActivity.newIntent(context, spaceId), false)
    }

    override fun performDeviceVerification(context: Context, otherUserId: String, sasTransactionId: String) {
        val session = sessionHolder.getSafeActiveSession() ?: return
        val tx = session.cryptoService().verificationService().getExistingTransaction(otherUserId, sasTransactionId)
                ?: return
        (tx as? IncomingSasVerificationTransaction)?.performAccept()
        if (context is AppCompatActivity) {
            VerificationBottomSheet.withArgs(
                    roomId = null,
                    otherUserId = otherUserId,
                    transactionId = sasTransactionId
            ).show(context.supportFragmentManager, "REQPOP")
        }
    }

    override fun requestSessionVerification(context: Context, otherSessionId: String) {
        val session = sessionHolder.getSafeActiveSession() ?: return
        val pr = session.cryptoService().verificationService().requestKeyVerification(
                supportedVerificationMethodsProvider.provide(),
                session.myUserId,
                listOf(otherSessionId)
        )
        if (context is AppCompatActivity) {
            VerificationBottomSheet.withArgs(
                    roomId = null,
                    otherUserId = session.myUserId,
                    transactionId = pr.transactionId
            ).show(context.supportFragmentManager, VerificationBottomSheet.WAITING_SELF_VERIF_TAG)
        }
    }

    override fun requestSelfSessionVerification(context: Context) {
        val session = sessionHolder.getSafeActiveSession() ?: return
        val otherSessions = session.cryptoService()
                .getCryptoDeviceInfo(session.myUserId)
                .filter { it.deviceId != session.sessionParams.deviceId }
                .map { it.deviceId }
        if (context is AppCompatActivity) {
            if (otherSessions.isNotEmpty()) {
                val pr = session.cryptoService().verificationService().requestKeyVerification(
                        supportedVerificationMethodsProvider.provide(),
                        session.myUserId,
                        otherSessions)
                VerificationBottomSheet.forSelfVerification(session, pr.transactionId ?: pr.localId)
                        .show(context.supportFragmentManager, VerificationBottomSheet.WAITING_SELF_VERIF_TAG)
            } else {
                VerificationBottomSheet.forSelfVerification(session)
                        .show(context.supportFragmentManager, VerificationBottomSheet.WAITING_SELF_VERIF_TAG)
            }
        }
    }

    override fun waitSessionVerification(context: Context) {
        val session = sessionHolder.getSafeActiveSession() ?: return
        if (context is AppCompatActivity) {
            VerificationBottomSheet.forSelfVerification(session)
                    .show(context.supportFragmentManager, VerificationBottomSheet.WAITING_SELF_VERIF_TAG)
        }
    }

    override fun upgradeSessionSecurity(context: Context, initCrossSigningOnly: Boolean) {
        if (context is AppCompatActivity) {
            BootstrapBottomSheet.show(
                    context.supportFragmentManager,
                    if (initCrossSigningOnly) SetupMode.CROSS_SIGNING_ONLY else SetupMode.NORMAL
            )
        }
    }

    override fun openGroupDetail(groupId: String, context: Context, buildTask: Boolean) {
        if (context is AppBaseActivity<*>) {
            context.notImplemented("Open group detail")
        } else {
            context.toast(R.string.not_implemented)
        }
    }

    override fun openRoomMemberProfile(userId: String, roomId: String?, context: Context, buildTask: Boolean) {
//        val args = RoomMemberProfileArgs(userId = userId, roomId = roomId)
//        val intent = RoomMemberProfileActivity.newIntent(context, args)
//        startActivity(context, intent, buildTask)
    }

    override fun openRoomForSharingAndFinish(activity: Activity, roomId: String, sharedData: SharedData) {
        val args = RoomDetailArgs(roomId, null, sharedData)
        val intent = RoomDetailActivity.newIntent(activity, args)
        activity.startActivity(intent)
        activity.finish()
    }

    override fun openRoomPreview(context: Context, publicRoom: PublicRoom, roomDirectoryData: RoomDirectoryData) {
        val intent = RoomPreviewActivity.newIntent(context, publicRoom, roomDirectoryData)
        context.startActivity(intent)
    }

    override fun openRoomPreview(context: Context, roomPreviewData: RoomPreviewData) {
        val intent = RoomPreviewActivity.newIntent(context, roomPreviewData)
        context.startActivity(intent)
    }

    override fun openMatrixToBottomSheet(context: Context, link: String) {
        if (context is AppCompatActivity) {
            val listener = object : MatrixToBottomSheet.InteractionListener {
                override fun navigateToRoom(roomId: String) {
                    openRoom(context, roomId)
                }

                override fun switchToSpace(spaceId: String) {
                    this@DefaultNavigator.switchToSpace(context, spaceId, Navigator.PostSwitchSpaceAction.None)
                }
            }
            // TODO check if there is already one??
            MatrixToBottomSheet.withLink(link, listener)
                    .show(context.supportFragmentManager, "HA#MatrixToBottomSheet")
        }
    }

    override fun openRoomDirectory(context: Context, initialFilter: String) {
//        when (val groupingMethod = appStateHandler.getCurrentRoomGroupingMethod()) {
//            is RoomGroupingMethod.ByLegacyGroup -> {
//                // TODO should open list of rooms of this group
//                val intent = RoomDirectoryActivity.getIntent(context, initialFilter)
//                context.startActivity(intent)
//            }
//            is RoomGroupingMethod.BySpace       -> {
//                val selectedSpace = groupingMethod.space()
//                if (selectedSpace == null) {
//                    val intent = RoomDirectoryActivity.getIntent(context, initialFilter)
//                    context.startActivity(intent)
//                } else {
//                    SpaceExploreActivity.newIntent(context, selectedSpace.roomId).let {
//                        context.startActivity(it)
//                    }
//                }
//            }
//        }
    }

    override fun openCreateRoom(context: Context, initialName: String, parentSpace: String?, parentName: String?) {
        val intent = CreateRoomActivity.getIntent(context, initialName, parentSpace, parentName)
        context.startActivity(intent)
    }

    override fun openCreateChecklist(context: Context, roomId: String, title: String) {
        val intent = CreateChecklistActivity.getIntent(
                context,
                CreateChecklistArgs(roomId = roomId, title = title)
        )
        context.startActivity(intent)
    }

    override fun openInviteUsersToRoom(context: Context, roomId: String) {
        val intent = InviteUsersToRoomActivity.getIntent(context, roomId)
        context.startActivity(intent)
    }

    override fun openRoomsFiltering(context: Context) {
        val intent = FilteredRoomsActivity.newIntent(context)
        context.startActivity(intent)
    }

    override fun openSettings(context: Context, directAccess: Int) {
        val intent = AppSettingsActivity.getIntent(context, directAccess)
        context.startActivity(intent)
    }

    override fun openKeysBackupSetup(context: Context, showManualExport: Boolean) {
        // if cross signing is enabled and trusted or not set up at all we should propose full 4S
        sessionHolder.getSafeActiveSession()?.let { session ->
            if (session.cryptoService().crossSigningService().getMyCrossSigningKeys() == null
                    || session.cryptoService().crossSigningService().canCrossSign()) {
                (context as? AppCompatActivity)?.let {
                    BootstrapBottomSheet.show(it.supportFragmentManager, SetupMode.NORMAL)
                }
            } else {
                context.startActivity(KeysBackupSetupActivity.intent(context, showManualExport))
            }
        }
    }

    override fun open4SSetup(context: Context, setupMode: SetupMode) {
        if (context is AppCompatActivity) {
            BootstrapBottomSheet.show(context.supportFragmentManager, setupMode)
        }
    }

    override fun openKeysBackupManager(context: Context) {
        context.startActivity(KeysBackupManageActivity.intent(context))
    }

    override fun openRoomProfile(context: Context, roomId: String, directAccess: Int?) {
        context.startActivity(RoomProfileActivity.newIntent(context, roomId, directAccess))
    }

    override fun openBigImageViewer(activity: Activity, sharedElement: View?, mxcUrl: String?, title: String?) {
        mxcUrl
                ?.takeIf { it.isNotBlank() }
                ?.let { avatarUrl ->
                    val intent = BigImageViewerActivity.newIntent(activity, title, avatarUrl)
                    val options = sharedElement?.let {
                        ActivityOptionsCompat.makeSceneTransitionAnimation(activity, it, ViewCompat.getTransitionName(it) ?: "")
                    }
                    activity.startActivity(intent, options?.toBundle())
                }
    }

    override fun openTerms(context: Context,
                           activityResultLauncher: ActivityResultLauncher<Intent>,
                           serviceType: TermsService.ServiceType,
                           baseUrl: String,
                           token: String?) {
        val intent = ReviewTermsActivity.intent(context, serviceType, baseUrl, token)
        activityResultLauncher.launch(intent)
    }

    override fun openStickerPicker(context: Context,
                                   activityResultLauncher: ActivityResultLauncher<Intent>,
                                   roomId: String,
                                   widget: Widget) {
        val widgetArgs = widgetArgsBuilder.buildStickerPickerArgs(roomId, widget)
        val intent = WidgetActivity.newIntent(context, widgetArgs)
        activityResultLauncher.launch(intent)
    }

    override fun openIntegrationManager(context: Context,
                                        activityResultLauncher: ActivityResultLauncher<Intent>,
                                        roomId: String,
                                        integId: String?,
                                        screen: String?) {
//        val widgetArgs = widgetArgsBuilder.buildIntegrationManagerArgs(roomId, integId, screen)
//        val intent = WidgetActivity.newIntent(context, widgetArgs)
//        activityResultLauncher.launch(intent)
    }

    override fun openRoomWidget(context: Context, roomId: String, widget: Widget, options: Map<String, Any>?) {
//        val widgetArgs = widgetArgsBuilder.buildRoomWidgetArgs(roomId, widget)
//        context.startActivity(WidgetActivity.newIntent(context, widgetArgs))
    }

    override fun openMediaViewer(activity: Activity,
                                 roomId: String,
                                 mediaData: AttachmentData,
                                 view: View,
                                 inMemory: List<AttachmentData>,
                                 options: ((MutableList<Pair<View, String>>) -> Unit)?) {
        AppAttachmentViewerActivity.newIntent(activity,
                mediaData,
                roomId,
                mediaData.eventId,
                inMemory,
                ViewCompat.getTransitionName(view)).let { intent ->
            val pairs = ArrayList<Pair<View, String>>()
            activity.window.decorView.findViewById<View>(android.R.id.statusBarBackground)?.let {
                pairs.add(Pair(it, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME))
            }
            activity.window.decorView.findViewById<View>(android.R.id.navigationBarBackground)?.let {
                pairs.add(Pair(it, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME))
            }

            pairs.add(Pair(view, ViewCompat.getTransitionName(view) ?: ""))
            options?.invoke(pairs)

            val bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, *pairs.toTypedArray()).toBundle()
            activity.startActivity(intent, bundle)
        }
    }

    override fun openSearch(context: Context, roomId: String) {
        val intent = SearchActivity.newIntent(context, SearchArgs(roomId))
        context.startActivity(intent)
    }

    private fun startActivity(context: Context, intent: Intent, buildTask: Boolean) {
        if (buildTask) {
            val stackBuilder = TaskStackBuilder.create(context)
            stackBuilder.addNextIntentWithParentStack(intent)
            stackBuilder.startActivities()
        } else {
            context.startActivity(intent)
        }
    }
}
