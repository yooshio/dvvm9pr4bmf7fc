/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.item

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import io.yoosh.app.R
import io.yoosh.app.core.extensions.setAttributeTintedImageResource
import io.yoosh.app.databinding.ItemChecklistTaskBinding

class ChecklistTaskView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val views: ItemChecklistTaskBinding

    init {
        inflate(context, R.layout.item_checklist_task, this)
        views = ItemChecklistTaskBinding.bind(this)
    }

    fun render(state: ChecklistTaskViewState) {
        views.taskNameTextView.text = state.taskAnswer

        when (state) {
            is ChecklistTaskViewState.ChecklistSending -> renderChecklistSending()
            is ChecklistTaskViewState.ChecklistReady   -> renderChecklistReady(state)
        }
    }

    private fun renderChecklistSending() {
        views.taskCheckImageView.isVisible = false
        renderTaskSelection(false)
    }

    private fun renderChecklistReady(state: ChecklistTaskViewState.ChecklistReady) {
        views.taskCheckImageView.isVisible = true
        renderTaskSelection(state.isSelected)
    }

    private fun renderTaskSelection(isSelected: Boolean) {
        if (isSelected) {
            views.taskCheckImageView.setImageResource(R.drawable.ic_checkbox_on_green)
        } else {
            views.taskCheckImageView.setImageResource(R.drawable.ic_checkbox_off)
        }
    }
}
