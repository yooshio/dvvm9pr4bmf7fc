/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.checklist.create

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.material.appbar.MaterialToolbar
import io.yoosh.app.R
import io.yoosh.app.core.di.ScreenComponent
import io.yoosh.app.core.extensions.addFragment
import io.yoosh.app.core.platform.ToolbarConfigurable
import io.yoosh.app.databinding.ActivitySimpleBinding
import io.yoosh.app.features.analytics.AnalyticsActivity

class CreateChecklistActivity : AnalyticsActivity<ActivitySimpleBinding>(), ToolbarConfigurable {

    override fun getCoordinatorLayout() = views.coordinatorLayout

    override fun configure(toolbar: MaterialToolbar) {
        configureToolbar(toolbar)
    }

    override fun injectWith(injector: ScreenComponent) {
        injector.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val createChecklistArgs: CreateChecklistArgs? = intent?.extras?.getParcelable(EXTRA_CREATE_CHECKLIST_ARGS)

        if (isFirstCreation()) {
            addFragment(
                    R.id.simpleFragmentContainer,
                    CreateChecklistFragment::class.java,
                    createChecklistArgs
            )
        }
    }

    companion object {

        private const val EXTRA_CREATE_CHECKLIST_ARGS = "EXTRA_CREATE_CHECKLIST_ARGS"

        fun getIntent(context: Context, createChecklistArgs: CreateChecklistArgs): Intent {
            return Intent(context, CreateChecklistActivity::class.java).apply {
                putExtra(EXTRA_CREATE_CHECKLIST_ARGS, createChecklistArgs)
            }
        }
    }

    override fun getBinding() = ActivitySimpleBinding.inflate(layoutInflater)
}
