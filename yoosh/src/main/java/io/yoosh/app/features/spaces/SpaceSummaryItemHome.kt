/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.spaces

import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.AppEpoxyHolder
import io.yoosh.app.core.epoxy.AppEpoxyModel
import io.yoosh.app.core.resources.DimensionProvider
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.home.room.detail.timeline.MessageColorProvider
import org.matrix.android.sdk.api.util.MatrixItem

@EpoxyModelClass(layout = R.layout.item_space_home)
abstract class SpaceSummaryItemHome : AppEpoxyModel<SpaceSummaryItemHome.Holder>() {

    @EpoxyAttribute lateinit var avatarRenderer: AvatarRenderer
    @EpoxyAttribute lateinit var matrixItem: MatrixItem
    @EpoxyAttribute var listener: (() -> Unit)? = null
    @EpoxyAttribute var longClickListener: (() -> Unit)? = null
    @EpoxyAttribute var izSelected: Boolean = false
    @EpoxyAttribute lateinit var dimensionProvider: DimensionProvider
    @EpoxyAttribute lateinit var messageColorProvider: MessageColorProvider

    private val bigPadding by lazy {
        dimensionProvider.getDimension(R.dimen.space_background_big_padding).toInt()
    }

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.rootView.setOnClickListener { listener?.invoke() }
        holder.rootView.setOnLongClickListener { longClickListener?.invoke(); true }
        if (izSelected) {
            holder.groupNameView.text = matrixItem.displayName
            holder.groupNameView.isVisible = true
            holder.rootView.background.alpha = 50
            holder.rootView.setPadding(bigPadding, bigPadding, bigPadding, bigPadding)
        } else {
            holder.groupNameView.isVisible = false
            holder.rootView.background.alpha = 0
            holder.rootView.setPadding(bigPadding, bigPadding, bigPadding, bigPadding)
        }
        avatarRenderer.render(matrixItem, holder.avatarImageView)

        holder.rootView.background.alpha = if (izSelected) 255 else 0
    }

    override fun unbind(holder: Holder) {
        avatarRenderer.clear(holder.avatarImageView)
        super.unbind(holder)
    }

    class Holder : AppEpoxyHolder() {
        val avatarImageView by bind<ImageView>(R.id.groupAvatarImageView)
        val groupNameView by bind<TextView>(R.id.groupNameView)
        val rootView by bind<ConstraintLayout>(R.id.spaceItemContainer)
    }
}
