package io.yoosh.app.features.home.room.detail.composer


import android.view.MotionEvent
import io.yoosh.app.core.utils.DimensionConverter

class DraggableStateProcessor(
        dimensionConverter: DimensionConverter,
) {

    private val distanceToLock = dimensionConverter.dpToPx(48).toFloat()
    private val distanceToCancel = dimensionConverter.dpToPx(120).toFloat()
    private val rtlXMultiplier = 1

    private var firstX: Float = 0f
    private var firstY: Float = 0f
    private var lastDistanceX: Float = 0f
    private var lastDistanceY: Float = 0f

    fun initialize(event: MotionEvent) {
        firstX = event.rawX
        firstY = event.rawY
        lastDistanceX = 0F
        lastDistanceY = 0F
    }

    fun process(event: MotionEvent, draggingState: VoiceMessageRecorderView.DraggingState): VoiceMessageRecorderView.DraggingState {
        val currentX = event.rawX
        val currentY = event.rawY
        val distanceX = firstX - currentX
        val distanceY = firstY - currentY
        return draggingState.nextDragState(currentX, currentY, distanceX, distanceY).also {
            lastDistanceX = distanceX
            lastDistanceY = distanceY
        }
    }

    private fun VoiceMessageRecorderView.DraggingState.nextDragState(currentX: Float, currentY: Float, distanceX: Float, distanceY: Float): VoiceMessageRecorderView.DraggingState {
        return when (this) {
            VoiceMessageRecorderView.DraggingState.Ready                                  -> {
                when {
                    isDraggingToCancel(currentX, distanceX, distanceY) -> VoiceMessageRecorderView.DraggingState.Cancelling(distanceX)
                    isDraggingToLock(currentY, distanceX, distanceY)   -> VoiceMessageRecorderView.DraggingState.Locking(distanceY)
                    else                                               -> VoiceMessageRecorderView.DraggingState.Ready
                }
            }
            is VoiceMessageRecorderView.DraggingState.Cancelling -> {
                when {
                    isDraggingToLock(currentY, distanceX, distanceY) -> VoiceMessageRecorderView.DraggingState.Locking(distanceY)
                    shouldCancelRecording(distanceX)                 -> VoiceMessageRecorderView.DraggingState.Cancel
                    else                                             -> VoiceMessageRecorderView.DraggingState.Cancelling(distanceX)
                }
            }
            is VoiceMessageRecorderView.DraggingState.Locking    -> {
                when {
                    isDraggingToCancel(currentX, distanceX, distanceY) -> VoiceMessageRecorderView.DraggingState.Cancelling(distanceX)
                    shouldLockRecording(distanceY)                     -> VoiceMessageRecorderView.DraggingState.Lock
                    else                                               -> VoiceMessageRecorderView.DraggingState.Locking(distanceY)
                }
            }
            else                                                 -> {
                this
            }
        }
    }

    private fun isDraggingToLock(currentY: Float, distanceX: Float, distanceY: Float) = (currentY < firstY) &&
            distanceY > distanceX && distanceY > lastDistanceY

    private fun isDraggingToCancel(currentX: Float, distanceX: Float, distanceY: Float) = isDraggingHorizontal(currentX) &&
            distanceX > distanceY && distanceX > lastDistanceX

    private fun isDraggingHorizontal(currentX: Float) = (currentX < firstX && rtlXMultiplier == 1) || (currentX > firstX && rtlXMultiplier == -1)

    private fun shouldCancelRecording(distanceX: Float): Boolean {
        return distanceX >= distanceToCancel
    }

    private fun shouldLockRecording(distanceY: Float): Boolean {
        return distanceY >= distanceToLock
    }
}
