/*
 * Copyright 2020 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.roomprofile.settings

import com.airbnb.epoxy.TypedEpoxyController
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.dividerItem
import io.yoosh.app.core.epoxy.profiles.buildProfileAction
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.core.ui.list.verticalMarginItem
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.features.form.formEditTextItem
import io.yoosh.app.features.form.formEditableAvatarItem
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.home.room.detail.timeline.format.RoomHistoryVisibilityFormatter
import org.matrix.android.sdk.api.util.toMatrixItem
import javax.inject.Inject

class RoomSettingsController @Inject constructor(
        private val stringProvider: StringProvider,
        private val avatarRenderer: AvatarRenderer,
        private val dimensionConverter: DimensionConverter
) : TypedEpoxyController<RoomSettingsViewState>() {

    interface Callback {
        fun onAvatarDelete()
        fun onAvatarChange()
        fun onNameChanged(name: String)
    }

    var callback: Callback? = null

    init {
        setData(null)
    }

    override fun buildModels(data: RoomSettingsViewState?) {
        val roomSummary = data?.roomSummary?.invoke() ?: return
        val host = this

        formEditableAvatarItem {
            id("avatar")
            enabled(data.actionPermissions.canChangeAvatar)
            when (val avatarAction = data.avatarAction) {
                RoomSettingsViewState.AvatarAction.None            -> {
                    // Use the current value
                    avatarRenderer(host.avatarRenderer)
                    // We do not want to use the fallback avatar url, which can be the other user avatar, or the current user avatar.
                    matrixItem(roomSummary.toMatrixItem().updateAvatar(data.currentRoomAvatarUrl))
                }
                RoomSettingsViewState.AvatarAction.DeleteAvatar    -> imageUri(null)
                is RoomSettingsViewState.AvatarAction.UpdateAvatar -> imageUri(avatarAction.newAvatarUri)
            }
            clickListener { host.callback?.onAvatarChange() }
            deleteListener { host.callback?.onAvatarDelete() }
        }

        verticalMarginItem {
            id("margin")
            heightInPx(host.dimensionConverter.dpToPx(32))
        }

        formEditTextItem {
            id("name")
            enabled(data.actionPermissions.canChangeName)
            value(data.newName ?: roomSummary.displayName)
            hint(host.stringProvider.getString(R.string.channel_settings_name_hint))

            onTextChange { text ->
                host.callback?.onNameChanged(text)
            }
        }
    }
}
