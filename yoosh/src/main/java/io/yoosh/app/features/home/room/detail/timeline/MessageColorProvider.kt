/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline

import androidx.annotation.ColorInt
import io.yoosh.app.R
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.features.home.room.detail.timeline.helper.MatrixItemColorProvider
import org.matrix.android.sdk.api.session.room.send.SendState
import org.matrix.android.sdk.api.util.MatrixItem
import javax.inject.Inject

class MessageColorProvider @Inject constructor(
        private val colorProvider: ColorProvider,
        private val matrixItemColorProvider: MatrixItemColorProvider) {

    @ColorInt
    fun getMemberNameTextColor(matrixItem: MatrixItem): Int {
        return matrixItemColorProvider.getColor(matrixItem)
    }

    @ColorInt
    fun getMessageTextColor(sendState: SendState): Int {
        return colorProvider.getColorFromAttribute(R.attr.app_message_text_color)
    }

    @ColorInt
    fun getOtherMessageBgColor(): Int {
        return colorProvider.getColorFromAttribute(R.attr.app_message_other_bg_color)
    }

    @ColorInt
    fun getMineMessageBgColor(): Int {
        return colorProvider.getColorFromAttribute(R.attr.app_message_mine_bg_color)
    }
}
