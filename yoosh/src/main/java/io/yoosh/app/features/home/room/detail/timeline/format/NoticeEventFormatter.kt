/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.format

import io.yoosh.app.ActiveSessionDataSource
import io.yoosh.app.R
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.core.utils.TextUtils.shortenUserId
import io.yoosh.app.features.home.room.detail.timeline.helper.RoomSummariesHolder
import io.yoosh.app.features.roomprofile.permissions.RoleFormatter
import io.yoosh.app.features.settings.AppPreferences
import org.matrix.android.sdk.api.extensions.orFalse
import org.matrix.android.sdk.api.session.events.model.Event
import org.matrix.android.sdk.api.session.events.model.EventType
import org.matrix.android.sdk.api.session.events.model.toModel
import org.matrix.android.sdk.api.session.room.model.Membership
import org.matrix.android.sdk.api.session.room.model.RoomMemberContent
import org.matrix.android.sdk.api.session.room.model.RoomNameContent
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.create.RoomCreateContent
import org.matrix.android.sdk.api.session.room.timeline.TimelineEvent
import timber.log.Timber
import javax.inject.Inject

class NoticeEventFormatter @Inject constructor(
        private val activeSessionDataSource: ActiveSessionDataSource,
        private val roomHistoryVisibilityFormatter: RoomHistoryVisibilityFormatter,
        private val roleFormatter: RoleFormatter,
        private val vectorPreferences: AppPreferences,
        private val roomSummariesHolder: RoomSummariesHolder,
        private val sp: StringProvider
) {

    private val currentUserId: String?
        get() = activeSessionDataSource.currentValue?.orNull()?.myUserId

    private fun Event.isSentByCurrentUser() = senderId != null && senderId == currentUserId

    private fun RoomSummary?.isDm() = this?.isDirect.orFalse()

    fun format(timelineEvent: TimelineEvent): CharSequence? {
        val rs = roomSummariesHolder.get(timelineEvent.roomId)
        return when (val type = timelineEvent.root.getClearType()) {
//            EventType.STATE_ROOM_CREATE             -> formatRoomCreateEvent(timelineEvent.root, rs)
            EventType.STATE_ROOM_NAME               -> formatRoomNameEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_TOPIC              -> formatRoomTopicEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_AVATAR             -> formatRoomAvatarEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
            EventType.STATE_ROOM_MEMBER             -> formatRoomMemberEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName, rs)
//            EventType.STATE_ROOM_THIRD_PARTY_INVITE -> formatRoomThirdPartyInvite(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName, rs)
//            EventType.STATE_ROOM_ALIASES            -> formatRoomAliasesEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_CANONICAL_ALIAS    -> formatRoomCanonicalAliasEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_HISTORY_VISIBILITY ->
//                formatRoomHistoryVisibilityEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName, rs)
//            EventType.STATE_ROOM_SERVER_ACL         -> formatRoomServerAclEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_GUEST_ACCESS       -> formatRoomGuestAccessEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName, rs)
//            EventType.STATE_ROOM_ENCRYPTION         -> formatRoomEncryptionEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_WIDGET,
//            EventType.STATE_ROOM_WIDGET_LEGACY      -> formatWidgetEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.STATE_ROOM_TOMBSTONE          -> formatRoomTombstoneEvent(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName, rs)
//            EventType.STATE_ROOM_POWER_LEVELS       -> formatRoomPowerLevels(timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.CALL_INVITE,
//            EventType.CALL_CANDIDATES,
//            EventType.CALL_HANGUP,
//            EventType.CALL_REJECT,
//            EventType.CALL_ANSWER                   -> formatCallEvent(type, timelineEvent.root, timelineEvent.senderInfo.disambiguatedDisplayName)
//            EventType.CALL_NEGOTIATE,
//            EventType.CALL_SELECT_ANSWER,
//            EventType.CALL_REPLACES,
//            EventType.MESSAGE,
//            EventType.REACTION,
//            EventType.KEY_VERIFICATION_START,
//            EventType.KEY_VERIFICATION_CANCEL,
//            EventType.KEY_VERIFICATION_ACCEPT,
//            EventType.KEY_VERIFICATION_MAC,
//            EventType.KEY_VERIFICATION_DONE,
//            EventType.KEY_VERIFICATION_KEY,
//            EventType.KEY_VERIFICATION_READY,
//            EventType.STATE_SPACE_CHILD,
//            EventType.STATE_SPACE_PARENT,
//            EventType.REDACTION                     -> formatDebug(timelineEvent.root)
            else                                    -> {
                Timber.v("Type $type not handled by this formatter")
                null
            }
        }
    }

    fun format(event: Event, senderName: String?, rs: RoomSummary?): CharSequence? {
        return when (val type = event.getClearType()) {
            EventType.STATE_ROOM_NAME               -> formatRoomNameEvent(event, senderName)
            EventType.STATE_ROOM_MEMBER             -> formatRoomMemberEvent(event, senderName, rs)
            else                                    -> {
                Timber.v("Type $type not handled by this formatter")
                null
            }
        }
    }

    private fun formatRoomCreateEvent(event: Event, rs: RoomSummary?): CharSequence? {
        return event.getClearContent().toModel<RoomCreateContent>()
                ?.takeIf { it.creator.isNullOrBlank().not() }
                ?.let {
                    if (event.isSentByCurrentUser()) {
                        sp.getString(if (rs.isDm()) R.string.notice_direct_room_created_by_you else R.string.notice_channel_created_by_you)
                    } else {
                        sp.getString(if (rs.isDm()) R.string.notice_direct_room_created else R.string.notice_channel_created, it.creator)
                    }
                }
    }

    private fun formatRoomNameEvent(event: Event, senderName: String?): CharSequence? {
        val content = event.getClearContent().toModel<RoomNameContent>() ?: return null
        return if (content.name.isNullOrBlank()) {
            if (event.isSentByCurrentUser()) {
                sp.getString(R.string.notice_room_name_removed_by_you)
            } else {
                sp.getString(R.string.notice_room_name_removed, senderName)
            }
        } else {
            if (event.isSentByCurrentUser()) {
                sp.getString(R.string.notice_room_name_changed_by_you, content.name)
            } else {
                sp.getString(R.string.notice_room_name_changed, senderName, content.name)
            }
        }
    }

    private fun formatRoomMemberEvent(event: Event, senderName: String?, rs: RoomSummary?): String? {
        val eventContent: RoomMemberContent? = event.getClearContent().toModel()
        val prevEventContent: RoomMemberContent? = event.resolvedPrevContent().toModel()
        val isMembershipEvent = prevEventContent?.membership != eventContent?.membership
                || eventContent?.membership == Membership.LEAVE
        return if (isMembershipEvent) {
            buildMembershipNotice(event, senderName, eventContent, prevEventContent, rs)
        } else {
            buildProfileNotice(event, senderName, eventContent, prevEventContent)
        }
    }

    private fun buildProfileNotice(event: Event, senderName: String?, eventContent: RoomMemberContent?, prevEventContent: RoomMemberContent?): String {
        val displayText = StringBuilder()
        // Check display name has been changed
        val senderId = event.senderId?.shortenUserId()
        if (eventContent?.displayName != prevEventContent?.displayName) {
            val displayNameText = when {
                prevEventContent?.displayName.isNullOrEmpty() ->
                    if (event.isSentByCurrentUser()) {
                        sp.getString(R.string.notice_display_name_set_by_you, eventContent?.displayName)
                    } else {
                        sp.getString(R.string.notice_display_name_set, senderId, eventContent?.displayName)
                    }
                eventContent?.displayName.isNullOrEmpty()     ->
                    if (event.isSentByCurrentUser()) {
                        sp.getString(R.string.notice_display_name_removed_by_you, prevEventContent?.displayName)
                    } else {
                        sp.getString(R.string.notice_display_name_removed, senderId, prevEventContent?.displayName)
                    }
                else                                          ->
                    if (event.isSentByCurrentUser()) {
                        sp.getString(R.string.notice_display_name_changed_from_by_you, prevEventContent?.displayName, eventContent?.displayName)
                    } else {
                        sp.getString(R.string.notice_display_name_changed_from, senderId, prevEventContent?.displayName, eventContent?.displayName)
                    }
            }
            displayText.append(displayNameText)
        }
        // Check whether the avatar has been changed
        if (eventContent?.avatarUrl != prevEventContent?.avatarUrl) {
            val displayAvatarText = if (displayText.isNotEmpty()) {
                displayText.append(" ")
                sp.getString(R.string.notice_avatar_changed_too)
            } else {
                if (event.isSentByCurrentUser()) {
                    sp.getString(R.string.notice_avatar_url_changed_by_you)
                } else {
                    sp.getString(R.string.notice_avatar_url_changed, senderName)
                }
            }
            displayText.append(displayAvatarText)
        }
        if (displayText.isEmpty()) {
            displayText.append(
                    if (event.isSentByCurrentUser()) {
                        sp.getString(R.string.notice_member_no_changes_by_you)
                    } else {
                        sp.getString(R.string.notice_member_no_changes, senderName)
                    }
            )
        }
        return displayText.toString()
    }

    private fun buildMembershipNotice(event: Event,
                                      senderName: String?,
                                      eventContent: RoomMemberContent?,
                                      prevEventContent: RoomMemberContent?,
                                      rs: RoomSummary?): String? {
        val senderDisplayName = (senderName ?: event.senderId ?: "").shortenUserId()
        val targetDisplayName = (eventContent?.displayName ?: prevEventContent?.displayName ?: event.stateKey ?: "").shortenUserId()
        return when (eventContent?.membership) {
            Membership.INVITE -> {
                when {
                    eventContent.thirdPartyInvite != null -> {
                        val userWhoHasAccepted = (eventContent.thirdPartyInvite?.signed?.mxid ?: event.stateKey)?.shortenUserId()
                        val threePidDisplayName = (eventContent.thirdPartyInvite?.displayName ?: "").shortenUserId()
                        eventContent.safeReason?.let { reason ->
                            if (event.isSentByCurrentUser()) {
                                sp.getString(R.string.notice_room_third_party_registered_invite_with_reason_by_you, threePidDisplayName, reason)
                            } else {
                                sp.getString(R.string.notice_room_third_party_registered_invite_with_reason, userWhoHasAccepted, threePidDisplayName, reason)
                            }
                        } ?: if (event.isSentByCurrentUser()) {
                            sp.getString(R.string.notice_room_third_party_registered_invite_by_you, threePidDisplayName)
                        } else {
                            sp.getString(R.string.notice_room_third_party_registered_invite, userWhoHasAccepted, threePidDisplayName)
                        }
                    }
                    event.stateKey == currentUserId       ->
                        eventContent.safeReason?.let { reason ->
                            sp.getString(R.string.notice_room_invite_you_with_reason, senderDisplayName, reason)
                        } ?: sp.getString(R.string.notice_room_invite_you, senderDisplayName)
                    event.stateKey.isNullOrEmpty()        ->
                        if (event.isSentByCurrentUser()) {
                            eventContent.safeReason?.let { reason ->
                                sp.getString(R.string.notice_room_invite_no_invitee_with_reason_by_you, reason)
                            } ?: sp.getString(R.string.notice_room_invite_no_invitee_by_you)
                        } else {
                            eventContent.safeReason?.let { reason ->
                                sp.getString(R.string.notice_room_invite_no_invitee_with_reason, senderDisplayName, reason)
                            } ?: sp.getString(R.string.notice_room_invite_no_invitee, senderDisplayName)
                        }
                    else                                  ->
                        if (event.isSentByCurrentUser()) {
                            eventContent.safeReason?.let { reason ->
                                sp.getString(R.string.notice_room_invite_with_reason_by_you, targetDisplayName, reason)
                            } ?: sp.getString(R.string.notice_room_invite_by_you, targetDisplayName)
                        } else {
                            eventContent.safeReason?.let { reason ->
                                sp.getString(R.string.notice_room_invite_with_reason, senderDisplayName, targetDisplayName, reason)
                            } ?: sp.getString(R.string.notice_room_invite, senderDisplayName, targetDisplayName)
                        }
                }
            }
            Membership.JOIN   ->
                eventContent.safeReason?.let { reason ->
                    if (event.isSentByCurrentUser()) {
                        sp.getString(if (rs.isDm()) R.string.notice_direct_room_join_with_reason_by_you else R.string.notice_room_join_with_reason_by_you,
                                reason)
                    } else {
                        sp.getString(if (rs.isDm()) R.string.notice_direct_room_join_with_reason else R.string.notice_room_join_with_reason,
                                senderDisplayName, reason)
                    }
                } ?: run {
                    if (event.isSentByCurrentUser()) {
                        sp.getString(if (rs.isDm()) R.string.notice_direct_room_join_by_you else R.string.notice_room_join_by_you)
                    } else {
                        sp.getString(if (rs.isDm()) R.string.notice_direct_room_join else R.string.notice_room_join,
                                senderDisplayName)
                    }
                }
            Membership.LEAVE  ->
                // 2 cases here: this member may have left voluntarily or they may have been "left" by someone else ie. kicked
                if (event.senderId == event.stateKey) {
                    when (prevEventContent?.membership) {
                        Membership.INVITE ->
                            if (event.isSentByCurrentUser()) {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_reject_with_reason_by_you, reason)
                                } ?: sp.getString(R.string.notice_room_reject_by_you)
                            } else {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_reject_with_reason, senderDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_reject, senderDisplayName)
                            }
                        else              ->
                            eventContent.safeReason?.let { reason ->
                                if (event.isSentByCurrentUser()) {
                                    sp.getString(
                                            if (rs.isDm()) {
                                                R.string.notice_direct_room_leave_with_reason_by_you
                                            } else {
                                                R.string.notice_room_leave_with_reason_by_you
                                            },
                                            reason
                                    )
                                } else {
                                    sp.getString(if (rs.isDm()) R.string.notice_direct_room_leave_with_reason else R.string.notice_room_leave_with_reason,
                                            senderDisplayName, reason)
                                }
                            } ?: run {
                                if (event.isSentByCurrentUser()) {
                                    sp.getString(if (rs.isDm()) R.string.notice_direct_channel_leave_by_you else R.string.notice_room_leave_by_you)
                                } else {
                                    sp.getString(if (rs.isDm()) R.string.notice_direct_channel_leave else R.string.notice_channel_leave,
                                            senderDisplayName)
                                }
                            }
                    }
                } else {
                    when (prevEventContent?.membership) {
                        Membership.INVITE ->
                            if (event.isSentByCurrentUser()) {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_withdraw_with_reason_by_you, targetDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_withdraw_by_you, targetDisplayName)
                            } else {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_withdraw_with_reason, senderDisplayName, targetDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_withdraw, senderDisplayName, targetDisplayName)
                            }
                        Membership.LEAVE,
                        Membership.JOIN   ->
                            if (event.isSentByCurrentUser()) {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_kick_with_reason_by_you, targetDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_kick_by_you, targetDisplayName)
                            } else {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_kick_with_reason, senderDisplayName, targetDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_kick, senderDisplayName, targetDisplayName)
                            }
                        Membership.BAN    ->
                            if (event.isSentByCurrentUser()) {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_unban_with_reason_by_you, targetDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_unban_by_you, targetDisplayName)
                            } else {
                                eventContent.safeReason?.let { reason ->
                                    sp.getString(R.string.notice_room_unban_with_reason, senderDisplayName, targetDisplayName, reason)
                                } ?: sp.getString(R.string.notice_room_unban, senderDisplayName, targetDisplayName)
                            }
                        else              -> null
                    }
                }
            Membership.BAN    ->
                if (event.isSentByCurrentUser()) {
                    eventContent.safeReason?.let {
                        sp.getString(R.string.notice_room_ban_with_reason_by_you, targetDisplayName, it)
                    } ?: sp.getString(R.string.notice_room_ban_by_you, targetDisplayName)
                } else {
                    eventContent.safeReason?.let {
                        sp.getString(R.string.notice_room_ban_with_reason, senderDisplayName, targetDisplayName, it)
                    } ?: sp.getString(R.string.notice_room_ban, senderDisplayName, targetDisplayName)
                }
            Membership.KNOCK  ->
                if (event.isSentByCurrentUser()) {
                    eventContent.safeReason?.let { reason ->
                        sp.getString(R.string.notice_room_kick_with_reason_by_you, targetDisplayName, reason)
                    } ?: sp.getString(R.string.notice_room_kick_by_you, targetDisplayName)
                } else {
                    eventContent.safeReason?.let { reason ->
                        sp.getString(R.string.notice_room_kick_with_reason, senderDisplayName, targetDisplayName, reason)
                    } ?: sp.getString(R.string.notice_room_kick, senderDisplayName, targetDisplayName)
                }
            else              -> null
        }
    }

    fun formatRedactedEvent(event: Event): String {
        return (event
                .unsignedData
                ?.redactedEvent
                ?.content
                ?.get("reason") as? String)
                ?.takeIf { it.isNotBlank() }
                .let { reason ->
                    if (reason == null) {
                        if (event.isRedactedBySameUser()) {
                            sp.getString(R.string.event_redacted_by_user_reason)
                        } else {
                            sp.getString(R.string.event_redacted_by_admin_reason)
                        }
                    } else {
                        if (event.isRedactedBySameUser()) {
                            sp.getString(R.string.event_redacted_by_user_reason_with_reason, reason)
                        } else {
                            sp.getString(R.string.event_redacted_by_admin_reason_with_reason, reason)
                        }
                    }
                }
    }
}
