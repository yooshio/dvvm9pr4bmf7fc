/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.list

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.OnModelBuildFinishedListener
import com.airbnb.mvrx.args
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.LayoutManagerStateRestorer
import io.yoosh.app.core.extensions.cleanup
import io.yoosh.app.core.extensions.exhaustive
import io.yoosh.app.core.platform.AppBaseFragment
import io.yoosh.app.core.platform.StateView
import io.yoosh.app.core.resources.UserPreferencesProvider
import io.yoosh.app.core.utils.startSharePlainTextIntent
import io.yoosh.app.databinding.FragmentRoomListBinding
import io.yoosh.app.features.home.HomeActivitySharedAction
import io.yoosh.app.features.home.HomeSharedActionViewModel
import io.yoosh.app.features.home.RoomListDisplayMode
import io.yoosh.app.features.home.room.list.actions.RoomListActionsArgs
import io.yoosh.app.features.home.room.list.actions.RoomListQuickActionsBottomSheet
import io.yoosh.app.features.home.room.list.actions.RoomListQuickActionsSharedAction
import io.yoosh.app.features.home.room.list.actions.RoomListQuickActionsSharedActionViewModel
import io.yoosh.app.features.notifications.NotificationDrawerManager
import io.yoosh.app.space
import kotlinx.parcelize.Parcelize
import org.matrix.android.sdk.api.extensions.orTrue
import org.matrix.android.sdk.api.session.room.model.RoomSummary
import org.matrix.android.sdk.api.session.room.model.SpaceChildInfo
import org.matrix.android.sdk.api.session.room.model.tag.RoomTag
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import javax.inject.Inject

@Parcelize
data class RoomListParams(
        val displayMode: RoomListDisplayMode
) : Parcelable

class RoomListFragment @Inject constructor(
        private val pagedControllerFactory: RoomSummaryPagedControllerFactory,
        val roomListViewModelFactory: RoomListViewModel.Factory,
        private val notificationDrawerManager: NotificationDrawerManager,
        private val userPreferencesProvider: UserPreferencesProvider
) : AppBaseFragment<FragmentRoomListBinding>(),
        RoomListListener {

    private var modelBuildListener: OnModelBuildFinishedListener? = null
    private lateinit var sharedActionViewModel: RoomListQuickActionsSharedActionViewModel
    private val roomListViewModel: RoomListViewModel by fragmentViewModel()
    private lateinit var homeViewModel: HomeSharedActionViewModel
    private lateinit var stateRestorer: LayoutManagerStateRestorer
    private val roomListParams: RoomListParams by args()

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentRoomListBinding {
        return FragmentRoomListBinding.inflate(inflater, container, false)
    }

    data class SectionKey(
            val name: String,
            val isExpanded: Boolean,
            val notifyOfLocalEcho: Boolean
    )

    data class SectionAdapterInfo(
            var section: SectionKey,
            val sectionHeaderAdapter: SectionHeaderAdapter,
            val contentEpoxyController: EpoxyController
    )

    private val adapterInfosList = mutableListOf<SectionAdapterInfo>()
    private var concatAdapter: ConcatAdapter? = null
    private var pagedController: RoomSummaryPagedController? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        views.stateView.contentView = views.roomListView

        sharedActionViewModel = activityViewModelProvider.get(RoomListQuickActionsSharedActionViewModel::class.java)

        homeViewModel = activityViewModelProvider.get(HomeSharedActionViewModel::class.java)
        roomListViewModel.observeViewEvents {
            when (it) {
                is RoomListViewEvents.Loading                   -> showLoading(it.message)
                is RoomListViewEvents.Failure                   -> showFailure(it.throwable)
                is RoomListViewEvents.SelectRoom                -> handleSelectRoom(it)
                is RoomListViewEvents.Done                      -> Unit
                is RoomListViewEvents.NavigateToMxToBottomSheet -> handleShowMxToLink(it.link)
                is RoomListViewEvents.AddModels                 -> handleAddModels()
                is RoomListViewEvents.ShareRoom                 -> onShareRoom(it.permalink)
            }.exhaustive
        }

        sharedActionViewModel
                .observe()
                .subscribe { handleQuickActions(it) }
                .disposeOnDestroyView()

        homeViewModel
                .observe()
                .subscribe { action ->
                    when (action) {
                        is HomeActivitySharedAction.SelectSpace,
                        HomeActivitySharedAction.OpenRootSpaces -> pagedController?.spaceIdUpdated()
                        else                                    -> Unit

                    }
                }
                .disposeOnDestroyView()

        roomListViewModel.selectSubscribe(viewLifecycleOwner, RoomListViewState::roomMembershipChanges) { ms ->
            // it's for invites local echo
            adapterInfosList.filter { it.section.notifyOfLocalEcho }
                    .onEach {
                        (it.contentEpoxyController as? RoomSummaryPagedController)?.roomChangeMembershipStates = ms
                    }
        }
    }

    private fun handleAddModels() {
//        val isZero = views.roomListView.adapter?.itemCount == 0 // dirty hack
//        views.noChannels.isVisible = isZero
//        views.noChannelsText.isVisible = isZero
    }

    private fun refreshCollapseStates() {
        roomListViewModel.sections.forEachIndexed { index, roomsSection ->
            val actualBlock = adapterInfosList[index]
            val isRoomSectionExpanded = roomsSection.isExpanded.value.orTrue()
            if (actualBlock.section.isExpanded && !isRoomSectionExpanded) {
                // mark controller as collapsed
                actualBlock.contentEpoxyController.setCollapsed(true)
            } else if (!actualBlock.section.isExpanded && isRoomSectionExpanded) {
                // we must expand!
                actualBlock.contentEpoxyController.setCollapsed(false)
            }
            actualBlock.section = actualBlock.section.copy(
                    isExpanded = isRoomSectionExpanded
            )
            actualBlock.sectionHeaderAdapter.updateSection(
                    actualBlock.sectionHeaderAdapter.roomsSectionData.copy(isExpanded = isRoomSectionExpanded)
            )
        }
    }

    override fun showFailure(throwable: Throwable) {
        showErrorInSnackbar(throwable)
    }

    private fun handleShowMxToLink(link: String) {
        navigator.openMatrixToBottomSheet(requireContext(), link)
    }

    override fun onDestroyView() {
        adapterInfosList.onEach { it.contentEpoxyController.removeModelBuildListener(modelBuildListener) }
        adapterInfosList.clear()
        modelBuildListener = null
        views.roomListView.cleanup()
        // DO Cleanup listener on the ConcatAdapter's adapters?
        stateRestorer.clear()
//        views.createChatFabMenu.listener = null
        concatAdapter = null
        super.onDestroyView()
    }

    private fun handleSelectRoom(event: RoomListViewEvents.SelectRoom) {
        navigator.openRoom(requireActivity(), event.roomSummary.roomId)
    }

    fun filterRoomsWith(filter: String) {
        // Scroll the list to top
        views.roomListView.scrollToPosition(0)

        roomListViewModel.handle(RoomListAction.FilterWith(filter))
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        stateRestorer = LayoutManagerStateRestorer(layoutManager).register()
        views.roomListView.layoutManager = layoutManager
        views.roomListView.itemAnimator = RoomListAnimator()
        layoutManager.recycleChildrenOnDetach = true

        modelBuildListener = OnModelBuildFinishedListener { it.dispatchTo(stateRestorer) }

        val concatAdapter = ConcatAdapter()

        roomListViewModel.sections.forEach { section ->
            val sectionAdapter = SectionHeaderAdapter {
                roomListViewModel.handle(RoomListAction.ToggleSection(section))
            }.also {
                it.updateSection(SectionHeaderAdapter.RoomsSectionData(section.sectionName))
            }

            val contentAdapter =
                    when {
                        section.livePages != null     -> {
                            pagedControllerFactory.createRoomSummaryPagedController(roomListParams.displayMode)
                                    .also { controller ->
                                        section.livePages.observe(viewLifecycleOwner) { pl ->
                                            pagedController = controller
                                            pagedController?.appStateHandler = roomListViewModel.getAppStateHandler()
                                            controller.submitList(pl)
                                            sectionAdapter.updateSection(sectionAdapter.roomsSectionData.copy(
                                                    isHidden = pl.isEmpty(),
                                                    isLoading = false
                                            ))
                                            checkEmptyState()
                                        }
                                        section.notificationCount.observe(viewLifecycleOwner) { counts ->
                                            sectionAdapter.updateSection(sectionAdapter.roomsSectionData.copy(
                                                    notificationCount = counts.totalCount,
                                                    isHighlighted = counts.isHighlight
                                            ))
                                        }
                                        section.isExpanded.observe(viewLifecycleOwner) { _ ->
                                            refreshCollapseStates()
                                        }
                                        controller.listener = this
                                    }
                        }
                        section.liveSuggested != null -> {
                            pagedControllerFactory.createSuggestedRoomListController()
                                    .also { controller ->
                                        section.liveSuggested.observe(viewLifecycleOwner) { info ->
                                            controller.setData(info)
                                            sectionAdapter.updateSection(sectionAdapter.roomsSectionData.copy(
                                                    isHidden = info.rooms.isEmpty(),
                                                    isLoading = false
                                            ))
                                            checkEmptyState()
                                        }
                                        section.isExpanded.observe(viewLifecycleOwner) { _ ->
                                            refreshCollapseStates()
                                        }
                                        controller.listener = this
                                    }
                        }
                        else                          -> {
                            pagedControllerFactory.createRoomSummaryListController()
                                    .also { controller ->
                                        section.liveList?.observe(viewLifecycleOwner) { list ->

                                            controller.setData(list)
                                            sectionAdapter.updateSection(sectionAdapter.roomsSectionData.copy(
                                                    isHidden = list.isEmpty(),
                                                    isLoading = false))
                                            checkEmptyState()
                                        }
                                        section.notificationCount.observe(viewLifecycleOwner) { counts ->
                                            sectionAdapter.updateSection(sectionAdapter.roomsSectionData.copy(
                                                    notificationCount = counts.totalCount,
                                                    isHighlighted = counts.isHighlight
                                            ))
                                        }
                                        section.isExpanded.observe(viewLifecycleOwner) { _ ->
                                            refreshCollapseStates()
                                        }
                                        controller.listener = this
                                    }
                        }
                    }
            adapterInfosList.add(
                    SectionAdapterInfo(
                            SectionKey(
                                    name = section.sectionName,
                                    isExpanded = section.isExpanded.value.orTrue(),
                                    notifyOfLocalEcho = section.notifyOfLocalEcho
                            ),
                            sectionAdapter,
                            contentAdapter
                    )
            )
//            concatAdapter.addAdapter(sectionAdapter)
            concatAdapter.addAdapter(contentAdapter.adapter)
        }

        this.concatAdapter = concatAdapter
        views.roomListView.adapter = concatAdapter
    }

    override fun invalidate() = withState(roomListViewModel) { state ->
//            footerController.setData(state)
    }

    private fun handleQuickActions(quickAction: RoomListQuickActionsSharedAction) {
        when (quickAction) {
            is RoomListQuickActionsSharedAction.NotificationsAllNoisy -> {
                roomListViewModel.handle(RoomListAction.ChangeRoomNotificationState(quickAction.roomId, RoomNotificationState.ALL_MESSAGES_NOISY))
            }
            is RoomListQuickActionsSharedAction.NotificationsMute     -> {
                roomListViewModel.handle(RoomListAction.ChangeRoomNotificationState(quickAction.roomId, RoomNotificationState.MUTE))
            }
            is RoomListQuickActionsSharedAction.Settings              -> {
                navigator.openRoomProfile(requireActivity(), quickAction.roomId)
            }
            is RoomListQuickActionsSharedAction.Favorite              -> {
                roomListViewModel.handle(RoomListAction.ToggleTag(quickAction.roomId, RoomTag.ROOM_TAG_FAVOURITE))
                homeViewModel.post(HomeActivitySharedAction.FavsChanged)
            }
            is RoomListQuickActionsSharedAction.Leave                 -> {
                promptLeaveRoom(quickAction.roomId)
            }
            is RoomListQuickActionsSharedAction.Share                 -> {
                roomListViewModel.handle(RoomListAction.ShareRoom(quickAction.roomId))
            }
        }.exhaustive
    }

    private fun onShareRoom(permalink: String) {
        startSharePlainTextIntent(
                fragment = this,
                activityResultLauncher = null,
                chooserTitle = null,
                text = permalink
        )
    }

    private fun promptLeaveRoom(roomId: String) {
        val isPublicRoom = roomListViewModel.isPublicRoom(roomId)
        val message = buildString {
            append(getString(R.string.room_participants_leave_prompt_msg))
            if (!isPublicRoom) {
                append("\n\n")
                append(getString(R.string.room_participants_leave_private_warning))
            }
        }
        MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Vector_MaterialAlertDialog_Destructive)
                .setTitle(R.string.room_participants_leave_prompt_title)
                .setMessage(message)
                .setPositiveButton(R.string.delete) { _, _ ->
                    roomListViewModel.handle(RoomListAction.LeaveRoom(roomId))
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun checkEmptyState() {
        val shouldShowEmpty = adapterInfosList.all { it.sectionHeaderAdapter.roomsSectionData.isHidden } &&
                !adapterInfosList.any { it.sectionHeaderAdapter.roomsSectionData.isLoading }
        if (shouldShowEmpty) {
            val emptyState = when (roomListParams.displayMode) {
                RoomListDisplayMode.FILTERED -> {
                    StateView.State.Empty(
                        title = getString(R.string.room_list_filtered_empty),
                        message = getString(R.string.room_list_filtered_empty))
                }
                RoomListDisplayMode.FAVS        ->
                    StateView.State.Empty(
                        title = getString(R.string.room_list_people_empty_title),
                        image = ContextCompat.getDrawable(requireContext(), R.drawable.ic_no_favs),
                        isBigImage = true,
                        message = getString(R.string.no_favs)
                    )
                RoomListDisplayMode.ROOMS         ->
                    StateView.State.Empty(
                        title = getString(R.string.room_list_rooms_empty_title),
                        image = ContextCompat.getDrawable(requireContext(), R.drawable.ic_no_channels),
                        isBigImage = true,
                        message = getString(R.string.no_channels)
                    )
                else                              ->
                    // Always display the content in this mode, because if the footer
                    StateView.State.Content
            }
            views.stateView.state = emptyState
        } else {
            // is there something to show already?
            if (adapterInfosList.any { !it.sectionHeaderAdapter.roomsSectionData.isHidden }) {
                views.stateView.state = StateView.State.Content
            } else {
                views.stateView.state = StateView.State.Loading
            }
        }
    }

    // RoomSummaryController.Callback **************************************************************

    override fun onRoomClicked(room: RoomSummary) {
        roomListViewModel.handle(RoomListAction.SelectRoom(room))
    }

    override fun onRoomLongClicked(room: RoomSummary): Boolean {
        userPreferencesProvider.neverShowLongClickOnRoomHelpAgain()
        RoomListQuickActionsBottomSheet
                .newInstance(room.roomId, RoomListActionsArgs.Mode.FULL)
                .show(childFragmentManager, "ROOM_LIST_QUICK_ACTIONS")
        return true
    }

    override fun onAcceptRoomInvitation(room: RoomSummary) {
        notificationDrawerManager.clearMemberShipNotificationForRoom(room.roomId)
        roomListViewModel.handle(RoomListAction.AcceptInvitation(room))
    }

    override fun onJoinSuggestedRoom(room: SpaceChildInfo) {
        roomListViewModel.handle(RoomListAction.JoinSuggestedRoom(room.childRoomId, room.viaServers))
    }

    override fun onSuggestedRoomClicked(room: SpaceChildInfo) {
        roomListViewModel.handle(RoomListAction.ShowRoomDetails(room.childRoomId, room.viaServers))
    }

    override fun onAddModels() {
        roomListViewModel.handle(RoomListAction.AddModels)
    }

    override fun onRejectRoomInvitation(room: RoomSummary) {
        notificationDrawerManager.clearMemberShipNotificationForRoom(room.roomId)
        roomListViewModel.handle(RoomListAction.RejectInvitation(room))
    }
}

//class VerticalSpaceItemDecoration(private val space: Int) : RecyclerView.ItemDecoration() {
//
//    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
//        outRect.bottom = space
//
//        // Add top margin only for the first item to avoid double space between items
//        if (parent.getChildAdapterPosition(view) == 0) {
//            outRect.top = space
//        }
//
//        // the ugliest workaround
//        if (view.id == -1) {
//            if (parent.getChildAdapterPosition(view) != 0) outRect.top = 0
//            outRect.bottom = 0
//        }
//    }
//}
