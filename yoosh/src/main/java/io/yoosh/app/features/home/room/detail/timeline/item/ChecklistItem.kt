/*
 * Copyright 2020 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.item

import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.children
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import io.yoosh.app.R
import io.yoosh.app.features.home.room.detail.RoomDetailAction
import io.yoosh.app.features.home.room.detail.timeline.TimelineEventController
import org.matrix.android.sdk.api.session.room.model.message.ChecklistTask

@EpoxyModelClass(layout = R.layout.item_timeline_event_base)
abstract class ChecklistItem : AbsMessageItem<ChecklistItem.Holder>() {

    @EpoxyAttribute
    var title: String? = null

    @EpoxyAttribute
    var callback: TimelineEventController.Callback? = null

    @EpoxyAttribute
    var eventId: String? = null

    @EpoxyAttribute
    var rootEventId: String? = null

    @EpoxyAttribute
    var checklistSent: Boolean = false

    @EpoxyAttribute
    var tasks: List<ChecklistTask>? = null

    @EpoxyAttribute
    lateinit var taskViewStates: List<ChecklistTaskViewState>

    override fun bind(holder: Holder) {
        super.bind(holder)
        val relatedEventId = eventId ?: return

        renderSendState(holder.view, holder.titleTextView)

        holder.titleTextView.text = title

        while (holder.tasksContainer.childCount < taskViewStates.size) {
            holder.tasksContainer.addView(ChecklistTaskView(holder.view.context))
        }
        while (holder.tasksContainer.childCount > taskViewStates.size) {
            holder.tasksContainer.removeViewAt(0)
        }

        val views = holder.tasksContainer.children.toList().filterIsInstance<ChecklistTaskView>()

        taskViewStates.forEachIndexed { index, optionViewState ->
            views.getOrNull(index)?.let {
                it.render(optionViewState)
                it.setOnClickListener {
                    rootEventId?.let { rootId ->
                        callback?.onTimelineItemAction(RoomDetailAction.UpdateTaskInChecklist(relatedEventId, rootId, optionViewState.taskId))
                    }
                }
            }
        }
    }

    class Holder : AbsMessageItem.Holder(STUB_ID) {
        val titleTextView by bind<TextView>(R.id.titleTextView)
        val tasksContainer by bind<LinearLayout>(R.id.tasksContainer)
    }

    companion object {
        private const val STUB_ID = R.id.messageContentChecklistStub
    }
}
