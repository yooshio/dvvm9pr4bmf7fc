/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.checklist.create

import android.view.inputmethod.EditorInfo
import com.airbnb.epoxy.EpoxyController
import io.yoosh.app.R
import io.yoosh.app.core.ui.list.ItemStyle
import io.yoosh.app.core.ui.list.genericButtonItem
import io.yoosh.app.core.ui.list.genericItem
import io.yoosh.app.features.form.formEditTextItem
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.features.form.formEditTextWithDeleteItem
import timber.log.Timber
import javax.inject.Inject

class CreateChecklistController @Inject constructor(
        private val stringProvider: StringProvider,
        private val colorProvider: ColorProvider
) : EpoxyController() {

    private var state: CreateChecklistViewState? = null
    var callback: Callback? = null

    fun setData(state: CreateChecklistViewState) {
        this.state = state
        requestModelBuild()
    }

    override fun buildModels() {
        val currentState = state ?: return
        val host = this

        genericItem {
            id("title_title")
            style(ItemStyle.BIG_TEXT)
            title(host.stringProvider.getString(R.string.create_checklist_title_title))
        }

        val titleImeAction = if (currentState.tasks.isEmpty()) EditorInfo.IME_ACTION_DONE else EditorInfo.IME_ACTION_NEXT

        formEditTextItem {
            id("title")
            value(currentState.title)
            hint(host.stringProvider.getString(R.string.create_checklist_title_hint))
            singleLine(true)
            selectAllOnFocus(true)
            imeOptions(titleImeAction)
            maxLength(40)
            onTextChange {
                host.callback?.onTitleChanged(it)
            }
        }

        genericItem {
            id("tasks_title")
            style(ItemStyle.BIG_TEXT)
            title(host.stringProvider.getString(R.string.create_checklist_tasks_title))
        }

        currentState.tasks.forEachIndexed { index, task ->
            val imeOptions = if (index == currentState.tasks.size - 1) EditorInfo.IME_ACTION_DONE else EditorInfo.IME_ACTION_NEXT
            formEditTextWithDeleteItem {
                id("task_$index")
                value(task)
                hint(host.stringProvider.getString(R.string.create_checklist_tasks_hint, (index + 1)))
                singleLine(true)
                imeOptions(imeOptions)
                maxLength(60)
                onTextChange {
                    host.callback?.onTaskChanged(index, it)
                }
                onDeleteClicked {
                    host.callback?.onDeleteTask(index)
                }
            }
        }

        if (currentState.canAddMoreTasks) {
            genericButtonItem {
                id("add_task")
                text(host.stringProvider.getString(R.string.create_checklist_add_task))
                textColor(host.colorProvider.getColor(R.color.black))
                buttonClickAction {
                    host.callback?.onAddTask()
                }
            }
        }
    }

    interface Callback {
        fun onTitleChanged(title: String)
        fun onTaskChanged(index: Int, task: String)
        fun onDeleteTask(index: Int)
        fun onAddTask()
    }
}
