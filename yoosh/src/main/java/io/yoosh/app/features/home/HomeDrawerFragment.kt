/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import io.yoosh.app.R
import io.yoosh.app.core.extensions.observeK
import io.yoosh.app.core.platform.AppBaseFragment
import io.yoosh.app.core.utils.TextUtils.shortenUserId
import io.yoosh.app.core.utils.openUrlInExternalBrowser
import io.yoosh.app.core.utils.startSharePlainTextIntent
import io.yoosh.app.databinding.FragmentHomeDrawerBinding
import io.yoosh.app.features.settings.AppSettingsActivity
import io.yoosh.app.features.workers.signout.SignOutUiWorker

import org.matrix.android.sdk.api.session.Session
import org.matrix.android.sdk.api.util.toMatrixItem
import tech.story.storyview.StoryView
import tech.story.storyview.model.ImageStory
import tech.story.storyview.model.SimpleStory
import tech.story.storyview.model.Story
import java.util.ArrayList
import javax.inject.Inject

class HomeDrawerFragment @Inject constructor(
        private val session: Session,
        private val avatarRenderer: AvatarRenderer
) : AppBaseFragment<FragmentHomeDrawerBinding>() {

    private lateinit var sharedActionViewModel: HomeSharedActionViewModel

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentHomeDrawerBinding {
        return FragmentHomeDrawerBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedActionViewModel = activityViewModelProvider.get(HomeSharedActionViewModel::class.java)

        session.getUserLive(session.myUserId).observeK(viewLifecycleOwner) { optionalUser ->
            val user = optionalUser?.getOrNull()
            if (user != null) {
                avatarRenderer.render(user.toMatrixItem(), views.homeDrawerHeaderAvatarView)
                views.homeDrawerUsernameView.text = user.displayName
                views.homeDrawerUserIdView.text = user.userId.shortenUserId()
            }
        }
        // Profile
        views.homeDrawerHeader.debouncedClicks {
            sharedActionViewModel.post(HomeActivitySharedAction.CloseDrawer)
            navigator.openSettings(requireActivity(), directAccess = AppSettingsActivity.EXTRA_DIRECT_ACCESS_GENERAL)
        }
        // Settings
        views.settingsAction.debouncedClicks {
            sharedActionViewModel.post(HomeActivitySharedAction.CloseDrawer)
            navigator.openSettings(requireActivity())
        }
        // Sign out
        views.homeDrawerHeaderSignoutView.debouncedClicks {
            sharedActionViewModel.post(HomeActivitySharedAction.CloseDrawer)
            SignOutUiWorker(requireActivity()).perform()
        }

        views.inviteFriendsAction.debouncedClicks {
            startSharePlainTextIntent(
                    fragment = this,
                    activityResultLauncher = null,
                    chooserTitle = getString(R.string.invite_friends),
                    text = getString(R.string.invite_friends_text, SHARE_URL),
                    extraTitle = getString(R.string.invite_friends_rich_title)
            )
        }

        views.feedbackAction.debouncedClicks {
            openUrlInExternalBrowser(requireContext(), FEEDBACK_URL)
        }

        views.introAction.debouncedClicks {
            showStories()
        }
    }

    private fun showStories() {
        val myStories: ArrayList<Story> = ArrayList<Story>()
        val story1: Story = ImageStory(
                getString(R.string.intro_story_1_image),
                getString(R.string.intro_story_1_title),
                getString(R.string.intro_story_1_desc)
        )

        val story2: Story = ImageStory(
                getString(R.string.intro_story_2_image),
                getString(R.string.intro_story_2_title),
                getString(R.string.intro_story_2_desc)
        )

        val story3: Story = ImageStory(
                getString(R.string.intro_story_3_image),
                getString(R.string.intro_story_3_title),
                getString(R.string.intro_story_3_desc)
        )

        val story4: Story = ImageStory(
                getString(R.string.intro_story_4_image),
                getString(R.string.intro_story_4_title),
                getString(R.string.intro_story_4_desc)
        )

        val story5: Story = ImageStory(
                getString(R.string.intro_story_5_image),
                getString(R.string.intro_story_5_title),
                getString(R.string.intro_story_5_desc)
        )

        val story6: Story = ImageStory(
                getString(R.string.intro_story_6_image),
                getString(R.string.intro_story_6_title),
                getString(R.string.intro_story_6_desc)
        )

        myStories.add(story1)
        myStories.add(story2)
        myStories.add(story3)
        myStories.add(story4)
        myStories.add(story5)
        myStories.add(story6)

        try {
            StoryView.Builder(requireActivity().supportFragmentManager)
                    .setStoriesList(myStories)
                    .setStoryDuration(5000)
                    .setTitleText(getString(R.string.intro_story_title))
                    .setStartingIndex(0)
                    .setBackground(ContextCompat.getDrawable(requireContext(), R.drawable.stories_background))
                    .build()
                    .show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        const val SHARE_URL = "https://yoosh.io/"
        const val FEEDBACK_URL = "https://yoosh.io/feedback"
    }
}
