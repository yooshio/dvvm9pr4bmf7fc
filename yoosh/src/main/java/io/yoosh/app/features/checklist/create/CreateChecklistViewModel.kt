/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.checklist.create

import com.airbnb.mvrx.FragmentViewModelContext
import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.yoosh.app.core.platform.AppViewModel
import org.matrix.android.sdk.api.session.Session
import timber.log.Timber

class CreateChecklistViewModel @AssistedInject constructor(
        @Assisted private val initialState: CreateChecklistViewState,
        private val session: Session
) : AppViewModel<CreateChecklistViewState, CreateChecklistAction, CreateChecklistViewEvents>(initialState) {

    private val room = session.getRoom(initialState.roomId)!!

    @AssistedFactory
    interface Factory {
        fun create(initialState: CreateChecklistViewState): CreateChecklistViewModel
    }

    companion object : MvRxViewModelFactory<CreateChecklistViewModel, CreateChecklistViewState> {

        const val MIN_TASKS_COUNT = 2
        const val MAX_TASKS_COUNT = 10

        @JvmStatic
        override fun create(viewModelContext: ViewModelContext, state: CreateChecklistViewState): CreateChecklistViewModel? {
            val fragment: CreateChecklistFragment = (viewModelContext as FragmentViewModelContext).fragment()

            return fragment.createChecklistViewModelFactory.create(state)
        }
    }

    init {
        observeState()
    }

    private fun observeState() {
        selectSubscribe(CreateChecklistViewState::title) {
            setState {
                copy(
                        canCreateChecklist = canCreateChecklist(it, this.tasks)
                )
            }
        }
        selectSubscribe(CreateChecklistViewState::tasks) {
            setState {
                copy(
                        canCreateChecklist = canCreateChecklist(this.title, this.tasks),
                        canAddMoreTasks = tasks.size < MAX_TASKS_COUNT
                )
            }
        }
    }

    override fun handle(action: CreateChecklistAction) {
        when (action) {
            CreateChecklistAction.OnCreateChecklist -> handleOnCreateChecklist()
            CreateChecklistAction.OnAddTask         -> handleOnAddTask()
            is CreateChecklistAction.OnDeleteTask   -> handleOnDeleteTask(action.index)
            is CreateChecklistAction.OnTaskChanged  -> handleOnTaskChanged(action.index, action.task)
            is CreateChecklistAction.OnTitleChanged -> handleOnTitleChanged(action.title)
        }
    }

    private fun handleOnCreateChecklist() = withState { state ->
        val nonEmptyTasks = state.tasks.filter { it.isNotEmpty() }
        when {
            state.title.isEmpty()                -> {
                _viewEvents.post(CreateChecklistViewEvents.EmptyTitleError)
            }
            nonEmptyTasks.size < MIN_TASKS_COUNT -> {
                _viewEvents.post(CreateChecklistViewEvents.NotEnoughTasksError(requiredTasksCount = MIN_TASKS_COUNT))
            }
            else                                 -> {
                room.sendChecklist(state.title, nonEmptyTasks)
                _viewEvents.post(CreateChecklistViewEvents.Success(nonEmptyTasks.size))
            }
        }
    }

    private fun handleOnAddTask() {
        setState {
            val extendedTasks = tasks + ""
            copy(
                    tasks = extendedTasks
            )
        }
    }

    private fun handleOnDeleteTask(index: Int) {
        setState {
            val filteredTasks = tasks.filterIndexed { ind, _ -> ind != index }
            copy(
                    tasks = filteredTasks
            )
        }
    }

    private fun handleOnTaskChanged(index: Int, task: String) {
        setState {
            val changedTasks = tasks.mapIndexed { ind, s -> if (ind == index) task else s }
            copy(
                    tasks = changedTasks
            )
        }
    }

    private fun handleOnTitleChanged(title: String) {
        setState {
            copy(
                    title = title
            )
        }
    }

    private fun canCreateChecklist(title: String, tasks: List<String>): Boolean {
        return title.isNotEmpty() &&
                tasks.filter { it.isNotEmpty() }.size >= MIN_TASKS_COUNT
    }
}
