package io.yoosh.app.features.analytics

sealed class AnalyticsEvent {
    enum class Basic(val eventName: String) {
        LOGIN_ATTEMPT("login_attempt"),
        LOGIN("login"),
        CREATE_CHANNEL("create_channel"),
        SEND_NOTE("send_note"),
        APP_OPEN("app_open"),
        OPEN_CREATE_CHANNEL("open_create_channel"),
        OPEN_CREATE_SPACE("open_create_space"),
        CREATE_CHECKLIST("create_checklist"),
    }
}
