/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.composer

import android.content.Context
import android.net.Uri
import android.text.Editable
import android.util.AttributeSet
import android.util.Log
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.text.toSpannable
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import io.yoosh.app.R
import io.yoosh.app.databinding.ComposerLayoutBinding

/**
 * Encapsulate the timeline composer UX.
 */
class TextComposerView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0) : ConstraintLayout(context, attrs, defStyleAttr) {

    interface Callback : ComposerEditText.Callback {
        fun onCloseRelatedMessage()
        fun onSendMessage(text: CharSequence)
        fun onAddAttachment()
        fun onCameraDirectSelected()
        fun onChecklistCreateSelected()
    }

    val views: ComposerLayoutBinding

    var callback: Callback? = null

    private var currentConstraintSetId: Int = -1

    private val animationDuration = 70L

    val text: Editable?
        get() = views.composerEditText.text

    private var leftSideVisible = true

    init {
        inflate(context, R.layout.composer_layout, this)
        views = ComposerLayoutBinding.bind(this)

        collapse(false)

        views.composerEditText.callback = object : ComposerEditText.Callback {
            override fun onRichContentSelected(contentUri: Uri): Boolean {
                return callback?.onRichContentSelected(contentUri) ?: false
            }

            override fun onTextBlankStateChanged(isBlank: Boolean) {
                callback?.onTextBlankStateChanged(isBlank)
                val shouldBeVisible = currentConstraintSetId == R.layout.composer_layout_constraint_set_expanded || !isBlank
                TransitionManager.endTransitions(this@TextComposerView)
                if (views.sendButton.isInvisible != !shouldBeVisible) {
                    TransitionManager.beginDelayedTransition(
                            this@TextComposerView,
                            AutoTransition().also { it.duration = animationDuration }
                    )
                    views.sendButton.isInvisible = !shouldBeVisible
                }
                views.attachmentButton.isVisible = isBlank
                views.cameraButton.isVisible = isBlank
                views.checklistButton.isVisible = isBlank
                leftSideVisible = isBlank
            }

            override fun onTextChanged(text: CharSequence) {
                callback?.onTextChanged(text)
            }
        }
        views.composerRelatedMessageCloseButton.setOnClickListener {
            collapse()
            callback?.onCloseRelatedMessage()
        }

        views.cameraButton.setOnClickListener {
            callback?.onCameraDirectSelected()
        }

        views.checklistButton.setOnClickListener {
            callback?.onChecklistCreateSelected()
        }

        views.sendButton.setOnClickListener {
            val textMessage = text?.toSpannable() ?: ""
            callback?.onSendMessage(textMessage)
        }

        views.attachmentButton.setOnClickListener {
            callback?.onAddAttachment()
        }
    }

    fun collapse(animate: Boolean = true, transitionComplete: (() -> Unit)? = null) {
        if (currentConstraintSetId == R.layout.composer_layout_constraint_set_compact) {
            // ignore we good
            return
        }
        currentConstraintSetId = R.layout.composer_layout_constraint_set_compact
        applyNewConstraintSet()
        views.sendButton.isInvisible = views.composerEditText.text.isNullOrEmpty()
    }

    fun expand(animate: Boolean = true, transitionComplete: (() -> Unit)? = null) {
        if (currentConstraintSetId == R.layout.composer_layout_constraint_set_expanded) {
            // ignore we good
            return
        }
        currentConstraintSetId = R.layout.composer_layout_constraint_set_expanded
        applyNewConstraintSet()
        views.sendButton.isInvisible = false
    }

    private fun applyNewConstraintSet() {
        ConstraintSet().also {
            it.clone(context, currentConstraintSetId)
            it.applyTo(this)
        }
    }

    fun toggleVisible(visible: Boolean) {
        if (!visible || (visible && leftSideVisible)) {
            views.attachmentButton.isVisible = visible
            views.cameraButton.isVisible = visible
            views.checklistButton.isVisible = visible
        }
        views.composerEditText.isVisible = visible
    }

    fun toggleEnable(enabled: Boolean) {
        views.composerEditText.typingEnabled = enabled
    }

    fun setRoomEncrypted(isEncrypted: Boolean) {
        if (isEncrypted) {
            views.composerEditText.setHint(R.string.room_message_placeholder)
        } else {
            views.composerEditText.setHint(R.string.room_message_placeholder)
        }
    }
}
