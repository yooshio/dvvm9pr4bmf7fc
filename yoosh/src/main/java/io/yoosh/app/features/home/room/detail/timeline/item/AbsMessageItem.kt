/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.home.room.detail.timeline.item

import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.ClickListener
import io.yoosh.app.core.resources.DimensionProvider
import io.yoosh.app.core.resources.MessageItemDimensionProvider
import io.yoosh.app.core.utils.TextUtils
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.home.room.detail.timeline.MessageColorProvider
import io.yoosh.app.features.home.room.detail.timeline.TimelineEventController
import timber.log.Timber

/**
 * Base timeline item that adds an optional information bar with the sender avatar, name, time, send state
 * Adds associated click listeners (on avatar, displayname)
 */
abstract class AbsMessageItem<H : AbsMessageItem.Holder> : AbsBaseMessageItem<H>() {

    override val baseAttributes: AbsBaseMessageItem.Attributes
        get() = attributes

    @EpoxyAttribute
    lateinit var attributes: Attributes

    private var lastClicked: Long = 0

    private val doubleListener = View.OnClickListener {
        val clicked = System.currentTimeMillis()
        if (clicked - lastClicked < 1000) {
            attributes.itemDoubleClickListener?.invoke(it)
        }
        lastClicked = clicked
    }

    override fun bind(holder: H) {
        super.bind(holder)
        val sentByMe = baseAttributes.informationData.sentByMe

        holder.rootView.gravity = if (sentByMe) Gravity.END else Gravity.START
        holder.rootView.setOnClickListener(doubleListener)

        val params = holder.messageContentRootView.layoutParams as? LinearLayout.LayoutParams
        params?.let {
            params.marginEnd = if (sentByMe) attributes.getSmallMargin() else attributes.getBigMargin()
            params.marginStart = if (sentByMe) attributes.getBigMargin() else attributes.getSmallMargin()

            val color = if (!sentByMe) attributes.getOtherColor() else attributes.getMineColor()
            val radius = attributes.messageDimensionProvider.getCornerRadius()
            val radiiArray = arrayListOf<Float>().also { repeat(8) { _ -> it.add(radius) } }

            if (sentByMe) radiiArray.apply { this[4] = 0f; this[5] = 0f } else radiiArray.apply { this[6] = 0f; this[7] = 0f }

            // quick hack to round needed corners
            val drawable = ((holder.messageContentRootView.background as? StateListDrawable)?.getStateDrawable(0) as? GradientDrawable)
            drawable?.mutate()
            drawable?.let {
                it.cornerRadii = radiiArray.toFloatArray()
                it.color = ColorStateList.valueOf(color)
            }
        }

        if (attributes.informationData.showInformation && !sentByMe) {
            holder.avatarImageView.layoutParams = holder.avatarImageView.layoutParams?.apply {
                height = attributes.avatarSize
                width = attributes.avatarSize
            }
            holder.avatarImageView.visibility = View.VISIBLE
            holder.memberNameView.visibility = View.VISIBLE
            holder.timeView.visibility = View.VISIBLE
            holder.timeView.text = attributes.informationData.time?.let { TextUtils.formatTimeAmericanToEuropean(it) }
            holder.memberNameView.text = attributes.informationData.memberName
            holder.memberNameView.setTextColor(attributes.getMemberNameColor())
            attributes.avatarRenderer.render(attributes.informationData.matrixItem, holder.avatarImageView)
            holder.avatarImageView.setOnLongClickListener(attributes.itemLongClickListener)
            holder.memberNameView.setOnLongClickListener(attributes.itemLongClickListener)
            holder.memberNameView.setOnClickListener(doubleListener)
        } else {
            holder.avatarImageView.setOnClickListener(null)
            holder.avatarImageView.visibility = View.GONE
            if (attributes.informationData.forceShowTimestamp) {
                holder.memberNameView.isInvisible = true
                holder.timeView.isVisible = true
                holder.timeView.text = attributes.informationData.time?.let { TextUtils.formatTimeAmericanToEuropean(it) }
            } else {
                holder.memberNameView.isVisible = false
                holder.timeView.isVisible = false
            }
            holder.avatarImageView.setImageResource(0)
            holder.avatarImageView.setOnLongClickListener(null)
            holder.memberNameView.setOnLongClickListener(null)
        }
    }

    override fun unbind(holder: H) {
        attributes.avatarRenderer.clear(holder.avatarImageView)
        holder.avatarImageView.setOnClickListener(null)
        holder.avatarImageView.setOnLongClickListener(null)
        holder.memberNameView.setOnClickListener(null)
        holder.memberNameView.setOnLongClickListener(null)
        super.unbind(holder)
    }

    private fun Attributes.getMemberNameColor() = messageColorProvider.getMemberNameTextColor(informationData.matrixItem)
    private fun Attributes.getSmallMargin() = attributes.messageDimensionProvider.getSmallMessageMargin()
    private fun Attributes.getBigMargin() = attributes.messageDimensionProvider.getBigMessageMargin()
    protected fun Attributes.getMineColor() = attributes.messageColorProvider.getMineMessageBgColor()
    protected fun Attributes.getOtherColor() = attributes.messageColorProvider.getOtherMessageBgColor()

    abstract class Holder(@IdRes stubId: Int) : AbsBaseMessageItem.Holder(stubId) {
        val avatarImageView by bind<ImageView>(R.id.messageAvatarImageView)
        val memberNameView by bind<TextView>(R.id.messageMemberNameView)
        val timeView by bind<TextView>(R.id.messageTimeView)
        val rootView by bind<LinearLayout>(R.id.rootView)
        val messageContentRootView by bind<RelativeLayout>(R.id.messageContentRootView)
    }

    /**
     * This class holds all the common attributes for timeline items.
     */
    data class Attributes(
            val avatarSize: Int,
            override val informationData: MessageInformationData,
            override val avatarRenderer: AvatarRenderer,
            override val messageColorProvider: MessageColorProvider,
            override val itemLongClickListener: View.OnLongClickListener? = null,
            override val itemClickListener: ClickListener? = null,
            override val itemDoubleClickListener: ClickListener? = null,
            val memberClickListener: ClickListener? = null,
            override val reactionPillCallback: TimelineEventController.ReactionPillCallback? = null,
            val avatarCallback: TimelineEventController.AvatarCallback? = null,
            override val readReceiptsCallback: TimelineEventController.ReadReceiptsCallback? = null,
            val emojiTypeFace: Typeface? = null,
            val dimensionProvider: DimensionProvider,
            val messageDimensionProvider: MessageItemDimensionProvider
    ) : AbsBaseMessageItem.Attributes {

        // Have to override as it's used to diff epoxy items
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Attributes

            if (avatarSize != other.avatarSize) return false
            if (informationData != other.informationData) return false

            return true
        }

        override fun hashCode(): Int {
            var result = avatarSize
            result = 31 * result + informationData.hashCode()
            return result
        }
    }
}
