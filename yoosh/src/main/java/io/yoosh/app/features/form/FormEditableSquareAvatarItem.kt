/*
 * Copyright (c) 2020 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.yoosh.app.features.form

import android.net.Uri
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.ClickListener
import io.yoosh.app.core.epoxy.AppEpoxyHolder
import io.yoosh.app.core.epoxy.onClick
import io.yoosh.app.core.glide.GlideApp
import io.yoosh.app.core.resources.DimensionProvider
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.features.home.AvatarRenderer
import org.matrix.android.sdk.api.util.MatrixItem

@EpoxyModelClass(layout = R.layout.item_editable_square_avatar)
abstract class FormEditableSquareAvatarItem : EpoxyModelWithHolder<FormEditableSquareAvatarItem.Holder>() {

    @EpoxyAttribute
    var avatarRenderer: AvatarRenderer? = null

    @EpoxyAttribute
    var matrixItem: MatrixItem? = null

    @EpoxyAttribute
    var enabled: Boolean = true

    @EpoxyAttribute
    var spaceTitle: String? = null

    @EpoxyAttribute
    var imageUri: Uri? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: ClickListener? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var deleteListener: ClickListener? = null

    @EpoxyAttribute
    lateinit var dimensionProvider: DimensionProvider

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.spaceTitle.text = spaceTitle
        holder.imageContainer.onClick(clickListener?.takeIf { enabled })
        when {
            imageUri != null   -> {
                val corner = dimensionProvider.getDimension(R.dimen.space_radius_big).toInt()
                GlideApp.with(holder.image)
                        .load(imageUri)
                        .transform(MultiTransformation(CenterCrop(), RoundedCorners(corner)))
                        .into(holder.image)
            }
            matrixItem != null -> {
                avatarRenderer?.render(matrixItem!!, holder.image, isSmallItem = false)
            }
            else               -> {
                avatarRenderer?.clear(holder.image)
            }
        }
        holder.delete.isVisible = enabled && (imageUri != null || matrixItem?.avatarUrl?.isNotEmpty() == true)
        holder.delete.onClick(deleteListener?.takeIf { enabled })
    }

    override fun unbind(holder: Holder) {
        avatarRenderer?.clear(holder.image)
        super.unbind(holder)
    }

    class Holder : AppEpoxyHolder() {
        val imageContainer by bind<View>(R.id.itemEditableAvatarImageContainer)
        val image by bind<ImageView>(R.id.itemEditableAvatarImage)
        val delete by bind<View>(R.id.itemEditableAvatarDelete)
        val spaceTitle by bind<TextView>(R.id.itemEditableSpaceTitle)
    }
}
