/*
 * Copyright (c) 2021 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.checklist.create

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import io.yoosh.app.R
import io.yoosh.app.core.extensions.configureWith
import io.yoosh.app.core.platform.AppBaseFragment
import io.yoosh.app.databinding.FragmentCreateChecklistBinding
import io.yoosh.app.features.analytics.AnalyticsEvent
import io.yoosh.app.features.analytics.AnalyticsFragment
import io.yoosh.app.features.checklist.create.CreateChecklistViewModel.Companion.MAX_TASKS_COUNT
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

@Parcelize
data class CreateChecklistArgs(
        val roomId: String,
        val title: String
) : Parcelable

class CreateChecklistFragment @Inject constructor(
        private val controller: CreateChecklistController,
        val createChecklistViewModelFactory: CreateChecklistViewModel.Factory,
) : AnalyticsFragment<FragmentCreateChecklistBinding>(), CreateChecklistController.Callback {

    private val viewModel: CreateChecklistViewModel by fragmentViewModel()

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentCreateChecklistBinding {
        return FragmentCreateChecklistBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appBaseActivity.setSupportActionBar(views.createChecklistToolbar)

        views.createChecklistRecyclerView.configureWith(controller, disableItemAnimation = true)
        views.createChecklistRecyclerView.setItemViewCacheSize(MAX_TASKS_COUNT + 4)
        controller.callback = this

        views.createChecklistClose.debouncedClicks {
            requireActivity().finish()
        }

        views.createChecklistButton.debouncedClicks {
            viewModel.handle(CreateChecklistAction.OnCreateChecklist)
        }

        viewModel.selectSubscribe(CreateChecklistViewState::canCreateChecklist) { canCreateChecklist ->
            views.createChecklistButton.isEnabled = canCreateChecklist
        }

        viewModel.selectSubscribe(CreateChecklistViewState::canCreateChecklist) { canCreateChecklist ->
            views.createChecklistButton.isEnabled = canCreateChecklist
        }

        viewModel.observeViewEvents {
            when (it) {
                is CreateChecklistViewEvents.Success             -> handleSuccess(it.numOfItems)
                CreateChecklistViewEvents.EmptyTitleError        -> handleEmptyTitleError()
                is CreateChecklistViewEvents.NotEnoughTasksError -> handleNotEnoughTasksError(it.requiredTasksCount)
            }
        }
    }

    override fun invalidate() = withState(viewModel) {
        controller.setData(it)
    }

    override fun onTitleChanged(title: String) {
        viewModel.handle(CreateChecklistAction.OnTitleChanged(title))
    }

    override fun onTaskChanged(index: Int, task: String) {
        viewModel.handle(CreateChecklistAction.OnTaskChanged(index, task))
    }

    override fun onDeleteTask(index: Int) {
        viewModel.handle(CreateChecklistAction.OnDeleteTask(index))
    }

    override fun onAddTask() {
        viewModel.handle(CreateChecklistAction.OnAddTask)
        // Scroll to bottom to show "Add Option" button
        views.createChecklistRecyclerView.apply {
            postDelayed({
                smoothScrollToPosition(adapter?.itemCount?.minus(1) ?: 0)
            }, 100)
        }
    }

    private fun handleSuccess(numOfItems: Int) {
        recordEvent(AnalyticsEvent.Basic.CREATE_CHECKLIST, mapOf("num_items" to numOfItems.toString()))
        requireActivity().finish()
    }

    private fun handleEmptyTitleError() {
        renderToast(getString(R.string.create_checklist_empty_title_error))
    }

    private fun handleNotEnoughTasksError(requiredOptionsCount: Int) {
        renderToast(
                resources.getQuantityString(
                        R.plurals.create_checklist_not_enough_tasks_error,
                        requiredOptionsCount,
                        requiredOptionsCount
                )
        )
    }

    private fun renderToast(message: String) {
        views.createChecklistToast.removeCallbacks(hideToastRunnable)
        views.createChecklistToast.text = message
        views.createChecklistToast.isVisible = true
        views.createChecklistToast.postDelayed(hideToastRunnable, 2_000)
    }

    private val hideToastRunnable = Runnable {
        views.createChecklistToast.isVisible = false
    }
}
