package io.yoosh.app.features.analytics

import android.view.View
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent

class EventRecorder(private val firebaseAnalytics: FirebaseAnalytics) : AnalyticsRecorder {

    override fun recordViewTouched(view: View) {
        try {
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                param(FirebaseAnalytics.Param.ITEM_NAME, view.id.toString())
            }
        } catch (e: Throwable) {
        }
    }

    override fun recordCustomEvent(name: String, values: Map<String, String>?) {
        try {
            firebaseAnalytics.logEvent(name) {
                values?.forEach {
                    param(it.key, it.value)
                }
            }
        } catch (e: Throwable) {
        }
    }

    override fun recordEvent(event: AnalyticsEvent.Basic) {
        try {
            firebaseAnalytics.logEvent(event.eventName, null)
        } catch (e: Throwable) {
        }
    }

    override fun recordEvent(event: AnalyticsEvent.Basic, values: Map<String, String>?) {
        try {
            firebaseAnalytics.logEvent(event.eventName) {
                values?.forEach {
                    param(it.key, it.value)
                }
            }
        } catch (e: Throwable) {
        }
    }

    override fun toggleAnalytics(enabled: Boolean) {
        try {
            firebaseAnalytics.setAnalyticsCollectionEnabled(enabled)
        } catch (e: Throwable) {
        }
    }
}
