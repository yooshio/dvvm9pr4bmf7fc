/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.roomdirectory.createroom

import com.airbnb.epoxy.TypedEpoxyController
import com.airbnb.mvrx.Loading
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.expandableTextItem
import io.yoosh.app.core.epoxy.infoTextItem
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.core.ui.list.verticalMarginItem
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.features.form.formEditTextItem
import io.yoosh.app.features.form.formEditableAvatarItem
import io.yoosh.app.features.form.formSubmitButtonItem
import io.yoosh.app.features.home.room.detail.timeline.item.messageTextItem
import javax.inject.Inject

class CreateRoomController @Inject constructor(
        private val stringProvider: StringProvider,
        private val dimensionConverter: DimensionConverter
) : TypedEpoxyController<CreateRoomViewState>() {

    var listener: Listener? = null

    var index = 0

    var parentName: String? = null

    override fun buildModels(viewState: CreateRoomViewState) {
        // display the form
        buildForm(viewState, viewState.asyncCreateRoomRequest !is Loading)
    }

    private fun buildForm(viewState: CreateRoomViewState, enableFormElement: Boolean) {
        val host = this
        verticalMarginItem {
            id("margin-0")
            heightInPx(host.dimensionConverter.dpToPx(16))
        }
        formEditableAvatarItem {
            id("avatar")
            enabled(enableFormElement)
            imageUri(viewState.avatarUri)
            clickListener { host.listener?.onAvatarChange() }
            deleteListener { host.listener?.onAvatarDelete() }
        }
        verticalMarginItem {
            id("margin")
            heightInPx(host.dimensionConverter.dpToPx(32))
        }
        formEditTextItem {
            id("name")
            enabled(enableFormElement)
            value(viewState.roomName)
            hint(host.stringProvider.getString(R.string.create_channel_name_hint))

            onTextChange { text ->
                host.listener?.onNameChange(text)
            }
        }

        val content = when (parentName) {
            null -> host.stringProvider.getString(R.string.channel_create_under_hub)
            else -> host.stringProvider.getString(R.string.channel_create_under_filter_hint, host.parentName)
        }

        infoTextItem {
            id("info")
            content(content)
        }
    }

    interface Listener {
        fun onAvatarDelete()
        fun onAvatarChange()
        fun onNameChange(newName: String)
    }
}
