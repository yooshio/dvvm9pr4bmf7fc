/*
 * Copyright (c) 2020 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.features.userdirectory

import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ScrollView
import androidx.core.view.forEach
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.airbnb.mvrx.activityViewModel
import com.airbnb.mvrx.args
import com.airbnb.mvrx.fragmentViewModel
import com.airbnb.mvrx.withState
import com.google.android.material.chip.Chip
import com.jakewharton.rxbinding3.widget.textChanges
import io.yoosh.app.R
import io.yoosh.app.core.extensions.cleanup
import io.yoosh.app.core.extensions.configureWith
import io.yoosh.app.core.extensions.hideKeyboard
import io.yoosh.app.core.extensions.setupAsSearch
import io.yoosh.app.core.platform.AppBaseFragment
import io.yoosh.app.core.platform.SimpleTextWatcher
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.core.utils.startSharePlainTextIntent
import io.yoosh.app.databinding.FragmentUserListBinding
import io.yoosh.app.features.homeserver.HomeServerCapabilitiesViewModel

import org.matrix.android.sdk.api.session.identity.ThreePid
import org.matrix.android.sdk.api.session.user.model.User
import javax.inject.Inject

class UserListFragment @Inject constructor(
        private val userListController: UserListController,
        private val dimensionConverter: DimensionConverter,
        val homeServerCapabilitiesViewModelFactory: HomeServerCapabilitiesViewModel.Factory
) : AppBaseFragment<FragmentUserListBinding>(),
        UserListController.Callback {

    private val args: UserListFragmentArgs by args()
    private val viewModel: UserListViewModel by activityViewModel()
    private lateinit var sharedActionViewModel: UserListSharedActionViewModel

    override fun getBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentUserListBinding {
        return FragmentUserListBinding.inflate(inflater, container, false)
    }

    override fun getMenuRes() = args.menuResId

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedActionViewModel = activityViewModelProvider.get(UserListSharedActionViewModel::class.java)
        if (args.showToolbar) {
            views.userListTitle.text = args.title
            appBaseActivity.setSupportActionBar(views.userListToolbar)
            setupCloseView()
            views.userListToolbar.isVisible = true
        } else {
            views.userListToolbar.isVisible = false
        }
        setupRecyclerView()
        setupSearchView()

        viewModel.selectSubscribe(this, UserListViewState::pendingSelections) {
            renderSelectedUsers(it)
        }

        viewModel.observeViewEvents {
            when (it) {
                is UserListViewEvents.OpenShareMatrixToLink -> {
                    val text = getString(R.string.invite_friends_text, it.link)
                    startSharePlainTextIntent(
                            fragment = this,
                            activityResultLauncher = null,
                            chooserTitle = getString(R.string.invite_friends),
                            text = text,
                            extraTitle = getString(R.string.invite_friends_rich_title)
                    )
                }
            }
        }
    }

    override fun onDestroyView() {
        views.userListRecyclerView.cleanup()
        super.onDestroyView()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        withState(viewModel) {
            val showMenuItem = it.pendingSelections.isNotEmpty()
            menu.forEach { menuItem ->
                menuItem.isVisible = showMenuItem
            }
        }
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = withState(viewModel) {
        sharedActionViewModel.post(UserListSharedAction.OnMenuItemSelected(item.itemId, it.pendingSelections))
        return@withState true
    }

    private fun setupRecyclerView() {
        userListController.callback = this
        // Don't activate animation as we might have way to much item animation when filtering
        views.userListRecyclerView.configureWith(userListController, disableItemAnimation = true)
    }

    private fun setupSearchView() {
        withState(viewModel) {
            views.userListSearch.hint = getString(R.string.user_directory_search_hint)
        }
        views.userListSearch
                .textChanges()
                .startWith(views.userListSearch.text)
                .subscribe { text ->
                    val searchValue = text.trim()
                    val action = if (searchValue.isBlank()) {
                        UserListAction.ClearSearchUsers
                    } else {
                        UserListAction.SearchUsers(searchValue.toString())
                    }
                    viewModel.handle(action)
                }
                .disposeOnDestroyView()

        views.userListSearch.apply {
            maxLines = 1
            inputType = InputType.TYPE_CLASS_TEXT
            imeOptions = EditorInfo.IME_ACTION_DONE
            setOnEditorActionListener { _, actionId, _ ->
                var consumed = false
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideKeyboard()
                    consumed = true
                    val username = text.toString()
                    if (username.isNotBlank())
                        viewModel.handle(UserListAction.AddPendingSelection(PendingSelection.UserPendingSelection(User(username)), false))
                }
                consumed
            }

            setOnTouchListener(View.OnTouchListener { _, event ->
                if (event.action == MotionEvent.ACTION_UP) {
                    if (event.rawX >= (this.right - this.compoundPaddingRight)) {
                        text = null
                        return@OnTouchListener true
                    }
                }
                return@OnTouchListener false
            })
        }
        views.userListSearch.requestFocus()
    }

    private fun setupCloseView() {
        views.userListClose.debouncedClicks {
            requireActivity().finish()
        }
    }

    override fun invalidate() = withState(viewModel) {
        userListController.setData(it)
    }

    private fun renderSelectedUsers(selections: Set<PendingSelection>) {
        invalidateOptionsMenu()

        val currentNumberOfChips = views.chipGroup.childCount
        val newNumberOfChips = selections.size

        views.chipGroup.removeAllViews()
        selections.forEach { addChipToGroup(it) }

        // Scroll to the bottom when adding chips. When removing chips, do not scroll
        if (newNumberOfChips >= currentNumberOfChips) {
            viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                views.chipGroupScrollView.fullScroll(ScrollView.FOCUS_DOWN)
            }
        }
    }

    private fun addChipToGroup(pendingSelection: PendingSelection) {
        val chip = Chip(requireContext())
        chip.setChipBackgroundColorResource(android.R.color.transparent)
        chip.chipStrokeWidth = dimensionConverter.dpToPx(1).toFloat()
        chip.text = pendingSelection.getBestName().ensurePlainId(true)
        chip.isClickable = true
        chip.isCheckable = false
        chip.isCloseIconVisible = true
        views.chipGroup.addView(chip)
        chip.setOnCloseIconClickListener {
            viewModel.handle(UserListAction.RemovePendingSelection(pendingSelection))
        }
    }

    fun getCurrentState() = withState(viewModel) { it }

    override fun onInviteFriendClick() {
        viewModel.handle(UserListAction.ComputeMatrixToLinkForSharing)
    }

    override fun onContactBookClick() {
        sharedActionViewModel.post(UserListSharedAction.OpenPhoneBook)
    }

    override fun onItemClick(user: User) {
        view?.hideKeyboard()
        viewModel.handle(UserListAction.AddPendingSelection(PendingSelection.UserPendingSelection(user)))
    }

    override fun onMatrixIdClick(matrixId: String) {
        view?.hideKeyboard()
        viewModel.handle(UserListAction.AddPendingSelection(PendingSelection.UserPendingSelection(User(matrixId))))
    }

    override fun onThreePidClick(threePid: ThreePid) {
        view?.hideKeyboard()
        viewModel.handle(UserListAction.AddPendingSelection(PendingSelection.ThreePidPendingSelection(threePid)))
    }

    override fun onUseQRCode() {
        view?.hideKeyboard()
        sharedActionViewModel.post(UserListSharedAction.AddByQrCode)
    }
}
