/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.yoosh.app.core.di

import android.content.Context
import android.content.res.Resources
import dagger.BindsInstance
import dagger.Component
import io.yoosh.app.ActiveSessionDataSource
import io.yoosh.app.AppStateHandler
import io.yoosh.app.EmojiCompatFontProvider
import io.yoosh.app.EmojiCompatWrapper
import io.yoosh.app.YooshApplication
import io.yoosh.app.core.dialogs.UnrecognizedCertificateDialog
import io.yoosh.app.core.error.ErrorFormatter
import io.yoosh.app.core.network.WifiDetector
import io.yoosh.app.core.pushers.PushersManager
import io.yoosh.app.core.time.Clock
import io.yoosh.app.core.utils.AssetReader
import io.yoosh.app.core.utils.DimensionConverter
import io.yoosh.app.features.configuration.AppConfiguration
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.home.CurrentSpaceSuggestedRoomListDataSource
import io.yoosh.app.features.home.room.detail.RoomDetailPendingActionStore
import io.yoosh.app.features.home.room.detail.timeline.helper.MatrixItemColorProvider
import io.yoosh.app.features.home.room.detail.timeline.helper.RoomSummariesHolder
import io.yoosh.app.features.home.room.detail.timeline.helper.VoiceMessagePlaybackTracker
import io.yoosh.app.features.html.EventHtmlRenderer
import io.yoosh.app.features.html.AppHtmlCompressor
import io.yoosh.app.features.invite.AutoAcceptInvites
import io.yoosh.app.features.login.ReAuthHelper
import io.yoosh.app.features.navigation.Navigator
import io.yoosh.app.features.notifications.NotifiableEventResolver
import io.yoosh.app.features.notifications.NotificationBroadcastReceiver
import io.yoosh.app.features.notifications.NotificationDrawerManager
import io.yoosh.app.features.notifications.NotificationUtils
import io.yoosh.app.features.notifications.PushRuleTriggerListener
import io.yoosh.app.features.popup.PopupAlertManager
import io.yoosh.app.features.reactions.data.EmojiDataSource
import io.yoosh.app.features.session.SessionListener
import io.yoosh.app.features.settings.AppPreferences
import io.yoosh.app.features.ui.UiStateRepository
import org.matrix.android.sdk.api.Matrix
import org.matrix.android.sdk.api.auth.AuthenticationService
import org.matrix.android.sdk.api.auth.HomeServerHistoryService
import org.matrix.android.sdk.api.raw.RawService
import org.matrix.android.sdk.api.session.Session
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface YooshComponent {

    fun inject(notificationBroadcastReceiver: NotificationBroadcastReceiver)

    fun inject(yooshApplication: YooshApplication)

    fun matrix(): Matrix

    fun matrixItemColorProvider(): MatrixItemColorProvider

    fun sessionListener(): SessionListener

    fun currentSession(): Session

    fun notificationUtils(): NotificationUtils

    fun notificationDrawerManager(): NotificationDrawerManager

    fun appContext(): Context

    fun resources(): Resources

    fun assetReader(): AssetReader

    fun dimensionConverter(): DimensionConverter

    fun appConfiguration(): AppConfiguration

    fun avatarRenderer(): AvatarRenderer

    fun activeSessionHolder(): ActiveSessionHolder

    fun unrecognizedCertificateDialog(): UnrecognizedCertificateDialog

    fun emojiCompatFontProvider(): EmojiCompatFontProvider

    fun emojiCompatWrapper(): EmojiCompatWrapper

    fun eventHtmlRenderer(): EventHtmlRenderer

    fun appHtmlCompressor(): AppHtmlCompressor

    fun navigator(): Navigator

    fun errorFormatter(): ErrorFormatter

    fun appStateHandler(): AppStateHandler

    fun currentSpaceSuggestedRoomListDataSource(): CurrentSpaceSuggestedRoomListDataSource

    fun roomDetailPendingActionStore(): RoomDetailPendingActionStore

    fun activeSessionObservableStore(): ActiveSessionDataSource

    fun authenticationService(): AuthenticationService

    fun rawService(): RawService

    fun homeServerHistoryService(): HomeServerHistoryService

    fun pushRuleTriggerListener(): PushRuleTriggerListener

    fun pusherManager(): PushersManager

    fun notifiableEventResolver(): NotifiableEventResolver

    fun appPreferences(): AppPreferences

    fun wifiDetector(): WifiDetector

    fun uiStateRepository(): UiStateRepository

    fun emojiDataSource(): EmojiDataSource

    fun alertManager(): PopupAlertManager

    fun reAuthHelper(): ReAuthHelper

    fun autoAcceptInvites(): AutoAcceptInvites

    fun roomSummaryHolder(): RoomSummariesHolder

    fun voiceMessagePlaybackTracker(): VoiceMessagePlaybackTracker

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance context: Context): YooshComponent
    }
}
