package io.yoosh.app.core.resources

import android.content.Context
import io.yoosh.app.R
import javax.inject.Inject

class MessageItemDimensionProvider @Inject constructor(private val context: Context) {

    private val cornerRadiusDp by lazy {
        context.resources.getDimension(R.dimen.bubble_message_radius)
    }
    private val messageMarginBig by lazy {
        context.resources.getDimension(R.dimen.bubble_message_margin_big).toInt()
    }

    private val messageMarginSmall by lazy {
        context.resources.getDimension(R.dimen.bubble_message_margin_small).toInt()
    }

    fun getCornerRadius(): Float {
        return cornerRadiusDp
    }

    fun getBigMessageMargin(): Int {
        return messageMarginBig
    }

    fun getSmallMessageMargin(): Int {
        return messageMarginSmall
    }
}
