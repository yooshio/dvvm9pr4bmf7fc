package io.yoosh.app.core.epoxy

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import io.yoosh.app.R

@EpoxyModelClass(layout = R.layout.item_info_text)
abstract class InfoTextItem : AppEpoxyModel<InfoTextItem.Holder>() {

    @EpoxyAttribute lateinit var content: String

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.content.text = content
    }

    class Holder : AppEpoxyHolder() {
        val content by bind<TextView>(R.id.info_text_content)
    }
}

