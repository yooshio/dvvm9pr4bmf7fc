/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package io.yoosh.app.core.epoxy.bottomsheet

import android.content.res.ColorStateList
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.TooltipCompat
import androidx.core.widget.ImageViewCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import io.yoosh.app.R
import io.yoosh.app.core.epoxy.ClickListener
import io.yoosh.app.core.epoxy.AppEpoxyHolder
import io.yoosh.app.core.epoxy.AppEpoxyModel
import io.yoosh.app.core.epoxy.onClick
import io.yoosh.app.core.extensions.setTextOrHide
import io.yoosh.app.core.resources.ColorProvider
import io.yoosh.app.core.resources.StringProvider
import io.yoosh.app.features.home.AvatarRenderer
import io.yoosh.app.features.themes.ThemeUtils
import org.matrix.android.sdk.api.session.room.notification.RoomNotificationState
import org.matrix.android.sdk.api.session.room.notification.invertEnabled
import org.matrix.android.sdk.api.util.MatrixItem

/**
 * A room preview for bottom sheet.
 */
@EpoxyModelClass(layout = R.layout.item_bottom_sheet_room_preview)
abstract class BottomSheetRoomPreviewItem : AppEpoxyModel<BottomSheetRoomPreviewItem.Holder>() {

    @EpoxyAttribute
    lateinit var avatarRenderer: AvatarRenderer

    @EpoxyAttribute
    lateinit var matrixItem: MatrixItem

    @EpoxyAttribute
    lateinit var stringProvider: StringProvider

    @EpoxyAttribute
    lateinit var colorProvider: ColorProvider

    @EpoxyAttribute
    var notificationState: RoomNotificationState = RoomNotificationState.ALL_MESSAGES_NOISY

    @EpoxyAttribute
    var izFavorite: Boolean = false

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var notificationClickListener: NotificationClickListener? = null

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var favoriteClickListener: ClickListener? = null

    override fun bind(holder: Holder) {
        super.bind(holder)
        avatarRenderer.render(matrixItem, holder.avatar)
        holder.roomName.setTextOrHide(matrixItem.displayName)
        setNotificationState(holder, notificationState)
        setFavoriteState(holder, izFavorite)

        holder.roomNotifications.onClick {
            // Immediate echo
            setNotificationState(holder, notificationState)
            // do the action
            notificationClickListener?.onClicked(notificationState.invertEnabled())
        }
        holder.roomFavorite.onClick {
            // Immediate echo
            setFavoriteState(holder, !izFavorite)
            // And do the action
            favoriteClickListener?.invoke(it)
        }
//        holder.roomSettings.apply {
//            onClick(settingsClickListener)
//            TooltipCompat.setTooltipText(this, stringProvider.getString(R.string.room_list_quick_actions_room_settings))
//        }
    }

    private fun setNotificationState(holder: Holder, notificationState: RoomNotificationState) {
        val description: String
        if (notificationState == RoomNotificationState.MUTE) {
            description =
                stringProvider.getString(R.string.room_list_quick_actions_notifications_mute)
            holder.roomNotifications.setImageResource(R.drawable.ic_bell)
        } else {
            description =
                stringProvider.getString(R.string.room_list_quick_actions_notifications_all_noisy)
            holder.roomNotifications.setImageResource(R.drawable.ic_bell_fill)
        }
        holder.roomNotifications.apply {
            contentDescription = description
            TooltipCompat.setTooltipText(this, description)
        }
    }

    private fun setFavoriteState(holder: Holder, isFavorite: Boolean) {
        val description: String
        if (isFavorite) {
            description = stringProvider.getString(R.string.room_list_quick_actions_favorite_remove)
            holder.roomFavorite.setImageResource(R.drawable.ic_stars_fill)
        } else {
            holder.roomFavorite.setImageResource(R.drawable.ic_stars)
            description = stringProvider.getString(R.string.room_list_quick_actions_favorite_add)
        }
        holder.roomFavorite.apply {
            contentDescription = description
            TooltipCompat.setTooltipText(this, description)
        }
    }

    class Holder : AppEpoxyHolder() {
        val avatar by bind<ImageView>(R.id.bottomSheetRoomPreviewAvatar)
        val roomName by bind<TextView>(R.id.bottomSheetRoomPreviewName)
        val roomNotifications by bind<ImageView>(R.id.bottomSheetRoomPreviewNotifications)
        val roomFavorite by bind<ImageView>(R.id.bottomSheetRoomPreviewFavorite)
    }

    interface NotificationClickListener {
        fun onClicked(enabled: Boolean)
    }
}
