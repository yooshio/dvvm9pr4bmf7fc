/*
 * Copyright 2019 New Vector Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.yoosh.app.core.di

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.yoosh.app.features.attachments.preview.AttachmentsPreviewFragment
import io.yoosh.app.features.crypto.keysbackup.settings.KeysBackupSettingsFragment
import io.yoosh.app.features.crypto.quads.SharedSecuredStorageKeyFragment
import io.yoosh.app.features.crypto.quads.SharedSecuredStoragePassphraseFragment
import io.yoosh.app.features.crypto.quads.SharedSecuredStorageResetAllFragment
import io.yoosh.app.features.crypto.recover.BootstrapConclusionFragment
import io.yoosh.app.features.crypto.recover.BootstrapConfirmPassphraseFragment
import io.yoosh.app.features.crypto.recover.BootstrapEnterPassphraseFragment
import io.yoosh.app.features.crypto.recover.BootstrapMigrateBackupFragment
import io.yoosh.app.features.crypto.recover.BootstrapReAuthFragment
import io.yoosh.app.features.crypto.recover.BootstrapSaveRecoveryKeyFragment
import io.yoosh.app.features.crypto.recover.BootstrapSetupRecoveryKeyFragment
import io.yoosh.app.features.crypto.recover.BootstrapWaitingFragment
import io.yoosh.app.features.crypto.verification.QuadSLoadingFragment
import io.yoosh.app.features.crypto.verification.cancel.VerificationCancelFragment
import io.yoosh.app.features.crypto.verification.cancel.VerificationNotMeFragment
import io.yoosh.app.features.crypto.verification.choose.VerificationChooseMethodFragment
import io.yoosh.app.features.crypto.verification.conclusion.VerificationConclusionFragment
import io.yoosh.app.features.crypto.verification.emoji.VerificationEmojiCodeFragment
import io.yoosh.app.features.crypto.verification.qrconfirmation.VerificationQRWaitingFragment
import io.yoosh.app.features.crypto.verification.qrconfirmation.VerificationQrScannedByOtherFragment
import io.yoosh.app.features.crypto.verification.request.VerificationRequestFragment
import io.yoosh.app.features.discovery.DiscoverySettingsFragment
import io.yoosh.app.features.discovery.change.SetIdentityServerFragment
import io.yoosh.app.features.home.HomeDetailFragment
import io.yoosh.app.features.home.HomeDrawerFragment
import io.yoosh.app.features.home.LoadingFragment
import io.yoosh.app.features.home.room.detail.RoomDetailFragment
import io.yoosh.app.features.home.room.detail.search.SearchFragment
import io.yoosh.app.features.home.room.list.RoomListFragment
import io.yoosh.app.features.login.LoginWebFragment
import io.yoosh.app.features.login.LoginWithProviderFragment
import io.yoosh.app.features.matrixto.MatrixToRoomSpaceFragment
import io.yoosh.app.features.matrixto.MatrixToUserFragment
import io.yoosh.app.features.checklist.create.CreateChecklistFragment
import io.yoosh.app.features.qrcode.QrCodeScannerFragment
import io.yoosh.app.features.reactions.EmojiChooserFragment
import io.yoosh.app.features.reactions.EmojiSearchResultFragment
import io.yoosh.app.features.roomdirectory.createroom.CreateRoomFragment
import io.yoosh.app.features.roomdirectory.roompreview.RoomPreviewNoPreviewFragment
import io.yoosh.app.features.roommemberprofile.RoomMemberProfileFragment
import io.yoosh.app.features.roommemberprofile.devices.DeviceListFragment
import io.yoosh.app.features.roommemberprofile.devices.DeviceTrustInfoActionFragment
import io.yoosh.app.features.roomprofile.RoomProfileFragment
import io.yoosh.app.features.roomprofile.members.RoomMemberListFragment
import io.yoosh.app.features.roomprofile.permissions.RoomPermissionsFragment
import io.yoosh.app.features.roomprofile.settings.RoomSettingsFragment
import io.yoosh.app.features.roomprofile.uploads.RoomUploadsFragment
import io.yoosh.app.features.roomprofile.uploads.files.RoomUploadsFilesFragment
import io.yoosh.app.features.roomprofile.uploads.media.RoomUploadsMediaFragment
import io.yoosh.app.features.settings.AppSettingsGeneralFragment
import io.yoosh.app.features.settings.AppSettingsHelpAboutFragment
import io.yoosh.app.features.settings.AppSettingsNotificationPreferenceFragment
import io.yoosh.app.features.settings.account.deactivation.DeactivateAccountFragment
import io.yoosh.app.features.settings.crosssigning.CrossSigningSettingsFragment
import io.yoosh.app.features.settings.devices.AppSettingsDevicesFragment
import io.yoosh.app.features.settings.homeserver.HomeserverSettingsFragment
import io.yoosh.app.features.settings.locale.LocalePickerFragment
import io.yoosh.app.features.settings.push.PushGatewaysFragment
import io.yoosh.app.features.settings.push.PushRulesFragment
import io.yoosh.app.features.settings.threepids.ThreePidsSettingsFragment
import io.yoosh.app.features.share.IncomingShareFragment
import io.yoosh.app.features.signout.soft.SoftLogoutFragment
import io.yoosh.app.features.spaces.SpaceHomeListFragment
import io.yoosh.app.features.spaces.create.CreateSpaceDetailsFragment
import io.yoosh.app.features.spaces.explore.SpaceDirectoryFragment
import io.yoosh.app.features.spaces.manage.SpaceAddRoomFragment
import io.yoosh.app.features.spaces.manage.SpaceManageRoomsFragment
import io.yoosh.app.features.spaces.manage.SpaceSettingsFragment
import io.yoosh.app.features.spaces.people.SpacePeopleFragment
import io.yoosh.app.features.spaces.preview.SpacePreviewFragment
import io.yoosh.app.features.terms.ReviewTermsFragment
import io.yoosh.app.features.usercode.ShowUserCodeFragment
import io.yoosh.app.features.userdirectory.UserListFragment
import io.yoosh.app.features.widgets.WidgetFragment

@Module
interface FragmentModule {
    /**
     * Fragments with @IntoMap will be injected by this factory
     */
    @Binds
    fun bindFragmentFactory(factory: AppFragmentFactory): FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(RoomListFragment::class)
    fun bindRoomListFragment(fragment: RoomListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LocalePickerFragment::class)
    fun bindLocalePickerFragment(fragment: LocalePickerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpaceHomeListFragment::class)
    fun bindSpaceHomeListFragment(fragment: SpaceHomeListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomDetailFragment::class)
    fun bindRoomDetailFragment(fragment: RoomDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CreateRoomFragment::class)
    fun bindCreateRoomFragment(fragment: CreateRoomFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CreateChecklistFragment::class)
    fun bindCreateChecklistFragment(fragment: CreateChecklistFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomPreviewNoPreviewFragment::class)
    fun bindRoomPreviewNoPreviewFragment(fragment: RoomPreviewNoPreviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(KeysBackupSettingsFragment::class)
    fun bindKeysBackupSettingsFragment(fragment: KeysBackupSettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LoadingFragment::class)
    fun bindLoadingFragment(fragment: LoadingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeDrawerFragment::class)
    fun bindHomeDrawerFragment(fragment: HomeDrawerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeDetailFragment::class)
    fun bindHomeDetailFragment(fragment: HomeDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EmojiSearchResultFragment::class)
    fun bindEmojiSearchResultFragment(fragment: EmojiSearchResultFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LoginWithProviderFragment::class)
    fun bindLoginWithProviderFragment(fragment: LoginWithProviderFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LoginWebFragment::class)
    fun bindLoginWebFragment(fragment: LoginWebFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(UserListFragment::class)
    fun bindUserListFragment(fragment: UserListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PushGatewaysFragment::class)
    fun bindPushGatewaysFragment(fragment: PushGatewaysFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AppSettingsNotificationPreferenceFragment::class)
    fun bindAppSettingsNotificationPreferenceFragment(fragment: AppSettingsNotificationPreferenceFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HomeserverSettingsFragment::class)
    fun bindHomeserverSettingsFragment(fragment: HomeserverSettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AppSettingsGeneralFragment::class)
    fun bindAppSettingsGeneralFragment(fragment: AppSettingsGeneralFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PushRulesFragment::class)
    fun bindPushRulesFragment(fragment: PushRulesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AppSettingsHelpAboutFragment::class)
    fun bindAppSettingsHelpAboutFragment(fragment: AppSettingsHelpAboutFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AppSettingsDevicesFragment::class)
    fun bindAppSettingsDevicesFragment(fragment: AppSettingsDevicesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ThreePidsSettingsFragment::class)
    fun bindThreePidsSettingsFragment(fragment: ThreePidsSettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomProfileFragment::class)
    fun bindRoomProfileFragment(fragment: RoomProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomMemberListFragment::class)
    fun bindRoomMemberListFragment(fragment: RoomMemberListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomUploadsFragment::class)
    fun bindRoomUploadsFragment(fragment: RoomUploadsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomUploadsMediaFragment::class)
    fun bindRoomUploadsMediaFragment(fragment: RoomUploadsMediaFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomUploadsFilesFragment::class)
    fun bindRoomUploadsFilesFragment(fragment: RoomUploadsFilesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomSettingsFragment::class)
    fun bindRoomSettingsFragment(fragment: RoomSettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomPermissionsFragment::class)
    fun bindRoomPermissionsFragment(fragment: RoomPermissionsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RoomMemberProfileFragment::class)
    fun bindRoomMemberProfileFragment(fragment: RoomMemberProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EmojiChooserFragment::class)
    fun bindEmojiChooserFragment(fragment: EmojiChooserFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SoftLogoutFragment::class)
    fun bindSoftLogoutFragment(fragment: SoftLogoutFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationRequestFragment::class)
    fun bindVerificationRequestFragment(fragment: VerificationRequestFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationChooseMethodFragment::class)
    fun bindVerificationChooseMethodFragment(fragment: VerificationChooseMethodFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationEmojiCodeFragment::class)
    fun bindVerificationEmojiCodeFragment(fragment: VerificationEmojiCodeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationQrScannedByOtherFragment::class)
    fun bindVerificationQrScannedByOtherFragment(fragment: VerificationQrScannedByOtherFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationQRWaitingFragment::class)
    fun bindVerificationQRWaitingFragment(fragment: VerificationQRWaitingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationConclusionFragment::class)
    fun bindVerificationConclusionFragment(fragment: VerificationConclusionFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationCancelFragment::class)
    fun bindVerificationCancelFragment(fragment: VerificationCancelFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(QuadSLoadingFragment::class)
    fun bindQuadSLoadingFragment(fragment: QuadSLoadingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerificationNotMeFragment::class)
    fun bindVerificationNotMeFragment(fragment: VerificationNotMeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(QrCodeScannerFragment::class)
    fun bindQrCodeScannerFragment(fragment: QrCodeScannerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DeviceListFragment::class)
    fun bindDeviceListFragment(fragment: DeviceListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DeviceTrustInfoActionFragment::class)
    fun bindDeviceTrustInfoActionFragment(fragment: DeviceTrustInfoActionFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CrossSigningSettingsFragment::class)
    fun bindCrossSigningSettingsFragment(fragment: CrossSigningSettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AttachmentsPreviewFragment::class)
    fun bindAttachmentsPreviewFragment(fragment: AttachmentsPreviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(IncomingShareFragment::class)
    fun bindIncomingShareFragment(fragment: IncomingShareFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapEnterPassphraseFragment::class)
    fun bindBootstrapEnterPassphraseFragment(fragment: BootstrapEnterPassphraseFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapConfirmPassphraseFragment::class)
    fun bindBootstrapConfirmPassphraseFragment(fragment: BootstrapConfirmPassphraseFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapWaitingFragment::class)
    fun bindBootstrapWaitingFragment(fragment: BootstrapWaitingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapSetupRecoveryKeyFragment::class)
    fun bindBootstrapSetupRecoveryKeyFragment(fragment: BootstrapSetupRecoveryKeyFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapSaveRecoveryKeyFragment::class)
    fun bindBootstrapSaveRecoveryKeyFragment(fragment: BootstrapSaveRecoveryKeyFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapConclusionFragment::class)
    fun bindBootstrapConclusionFragment(fragment: BootstrapConclusionFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapReAuthFragment::class)
    fun bindBootstrapReAuthFragment(fragment: BootstrapReAuthFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BootstrapMigrateBackupFragment::class)
    fun bindBootstrapMigrateBackupFragment(fragment: BootstrapMigrateBackupFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DeactivateAccountFragment::class)
    fun bindDeactivateAccountFragment(fragment: DeactivateAccountFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SharedSecuredStoragePassphraseFragment::class)
    fun bindSharedSecuredStoragePassphraseFragment(fragment: SharedSecuredStoragePassphraseFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SharedSecuredStorageKeyFragment::class)
    fun bindSharedSecuredStorageKeyFragment(fragment: SharedSecuredStorageKeyFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SharedSecuredStorageResetAllFragment::class)
    fun bindSharedSecuredStorageResetAllFragment(fragment: SharedSecuredStorageResetAllFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SetIdentityServerFragment::class)
    fun bindSetIdentityServerFragment(fragment: SetIdentityServerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DiscoverySettingsFragment::class)
    fun bindDiscoverySettingsFragment(fragment: DiscoverySettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ReviewTermsFragment::class)
    fun bindReviewTermsFragment(fragment: ReviewTermsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WidgetFragment::class)
    fun bindWidgetFragment(fragment: WidgetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SearchFragment::class)
    fun bindSearchFragment(fragment: SearchFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ShowUserCodeFragment::class)
    fun bindShowUserCodeFragment(fragment: ShowUserCodeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpacePreviewFragment::class)
    fun bindSpacePreviewFragment(fragment: SpacePreviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CreateSpaceDetailsFragment::class)
    fun bindCreateSpaceDetailsFragment(fragment: CreateSpaceDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MatrixToUserFragment::class)
    fun bindMatrixToUserFragment(fragment: MatrixToUserFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MatrixToRoomSpaceFragment::class)
    fun bindMatrixToRoomSpaceFragment(fragment: MatrixToRoomSpaceFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpaceDirectoryFragment::class)
    fun bindSpaceDirectoryFragment(fragment: SpaceDirectoryFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpaceAddRoomFragment::class)
    fun bindSpaceAddRoomFragment(fragment: SpaceAddRoomFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpacePeopleFragment::class)
    fun bindSpacePeopleFragment(fragment: SpacePeopleFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpaceSettingsFragment::class)
    fun bindSpaceSettingsFragment(fragment: SpaceSettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SpaceManageRoomsFragment::class)
    fun bindSpaceManageRoomsFragment(fragment: SpaceManageRoomsFragment): Fragment
}
