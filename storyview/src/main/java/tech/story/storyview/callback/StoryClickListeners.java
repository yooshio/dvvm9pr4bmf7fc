package tech.story.storyview.callback;

public interface StoryClickListeners {

    void onDescriptionClickListener(int position);

    void onTitleIconClickListener(int position);

}
