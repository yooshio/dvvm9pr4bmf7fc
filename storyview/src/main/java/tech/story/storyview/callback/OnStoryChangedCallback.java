package tech.story.storyview.callback;

public interface OnStoryChangedCallback {

    void storyChanged(int position);

}
