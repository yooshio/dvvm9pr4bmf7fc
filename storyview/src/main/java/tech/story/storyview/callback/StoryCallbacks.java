package tech.story.storyview.callback;

public interface StoryCallbacks {

    void startStories();

    void pauseStories();

    void nextStory();

    void onDescriptionClickListener(int position);

}
